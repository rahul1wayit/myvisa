<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
	<footer id="site-footer" role="contentinfo" class="header-footer-group">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-3 col-md">
                  <div class="footer-bx">
                     <?php dynamic_sidebar('footerlinks');?>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="footer-bx branches-bx">
                     <?php dynamic_sidebar('footerbranches');?>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="footer-bx">
                     <?php dynamic_sidebar('footerSA');?>
                  </div>
               </div>
               <div class="col-sm-12 col-md">
                  <div class="footer-bx location-bx">
                     <?php dynamic_sidebar('footerLocations');?>
                  </div>
               </div>
            </div>
         </div>
         <div class="copyright">
            <div class="container">
               <div class="copyright-text">
                  <p><?php dynamic_sidebar('footercopyright');?></p>
               </div>
            </div>
         </div>
      </footer><!-- #site-footer -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<?php wp_footer(); ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
 <script src="<?php echo  get_template_directory_uri();?>/assets/js/jquery.validate.min.js"></script>
 <script src="<?php echo  get_template_directory_uri();?>/assets/js/form-validate.js"></script>
<script>

jQuery('#banner').slick({
  autoplay: true,
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows : false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        infinite: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        dots: false
      }
    },
  ]
});

jQuery('#peopletalk').slick({
  autoplay: true,
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows : false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        infinite: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        dots: false
      }
    },
  ]
});

jQuery('#upcoming-webinar').slick({
  autoplay: true,
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows : false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        infinite: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        dots: false
      }
    },
  ]
});

jQuery('#Visa').slick({
  autoplay: true,
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows : false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        infinite: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        dots: false
      }
    },
  ]
});


jQuery('#testimonial').slick({
  autoplay: true,
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows : false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


jQuery('#ourpartnerlogos').slick({
  autoplay: true,
  dots: false,
  infinite: true,
  speed: 700,
  slidesToShow: 5,
  slidesToScroll: 1,
  arrows : false,
  responsive: [
     {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
    	slidesToShow: 2,
        slidesToScroll: 1,
        dots: false
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

</script>
<script>

$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<script>
   jQuery('#refusalcountry').change(function(){
       if (jQuery(this).val() != ''){

           jQuery('.nested-year').show();
       }else{

           jQuery('.nested-year').hide();
       }

   });

</script>
	</body>
</html>
