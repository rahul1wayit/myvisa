<?php
/**The Template Name: Home Page
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
$pid = get_the_ID();
?>
<style>
button.btn.dropdown-toggle.btn-light,button.btn.dropdown-toggle.bs-placeholder.btn-light {
    background: #fff;
    width: 234px;
    /* height: 67px; */
    padding: 16px;
    border: 1px solid #ced4da;
}
.has-error{color:red;}
.filter-option-inner-inner {
    color: #000;
    font-size: 14px;
}
.slick-dots li {
    display: none
}
.slick-dots li.slick-active,
.slick-dots li.slick-active + li,
.slick-dots li.slick-active + li + li {
    display: block;
}
.slick-dots li:nth-last-child(1),
.slick-dots li:nth-last-child(2),
.slick-dots li:nth-last-child(3) {
    display: block;
}
.slick-dots li.slick-active ~ li:nth-last-child(1),
.slick-dots li.slick-active ~ li:nth-last-child(2),
.slick-dots li.slick-active ~ li:nth-last-child(3) {
    display: none;
}
.slick-dots li.slick-active + li + li:nth-last-child(3),
.slick-dots li.slick-active + li + li:nth-last-child(2),
.slick-dots li.slick-active + li + li:nth-last-child(1),
.slick-dots li.slick-active + li:nth-last-child(3),
.slick-dots li.slick-active + li:nth-last-child(2),
.slick-dots li.slick-active + li:nth-last-child(1){
    display: block;
}

         .get-in-touch-form{display:none;}
        .get-in-touch-form input, .get-in-touch-form select {
            border: solid 2px transparent;
        }
img.vp-img{max-width:490px;}
		#ucPopUp{display:none;position: fixed;background: rgba(0,0,0,.1);top: 0;width: 100%;height: 100%;left: 0;bottom: 0;right: 0;z-index: 999;}
      </style>
      <!--<span id="ucPopUp"></span>-->
      <div class="under-construction-popup">
         <div class="container">
            <div class="popup-detail">
               <div class="popup-detail-info">
                   <a class="close ucpopup" href="#"><i class="fas fa-times"></i></a>
                  <div class="left-cont">
                     <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/07/logo.png">
                  </div>
                  <div class="right-cont">
                     <h1>Our Website is Coming Soon</h1>
                     <p>In the meantime connect with us with the information below</p>
                     <ul>
                        <li><span><i class="fas fa-phone"></i></span> <a href="tel:+91 0175 500 0007">+91 0175 500 0007</a>, <a href="tel:76118-76118">76118-76118</a></li>
                        <li><span><i class="fas fa-envelope"></i></span> <a href="mailto:enquiry@myvisa.co.in">enquiry@myvisa.co.in</a></li>
                        <li><span><i class="fas fa-home"></i></span> Fateh Complex, Walia Enclave,<br> Opp. Punjabi University, Patiala, Punjab. Patiala, India</li>
                     <li>
                          <ul>
                              <li><a target="_blank" href="https://www.facebook.com/officialmyvisa"><i class="fab fa-facebook-f"></i></a></li>
                              <li><a target="_blank" href="https://twitter.com/officialmyvisa"><i class="fab fa-twitter"></i></a></li>
                              <li><a target="_blank" href="https://www.instagram.com/officialmyvisa/"><i class="fab fa-instagram"></i></a></li>
                           </ul>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="get-btn">
                  <a class="get-in-touch-btn" href="#">Get in touch</a>
               </div>
            </div>
            <div class="get-in-touch-form">
                <div class="get-in-touch-detail">
                     <a class="close" href="#"><i class="fas fa-times"></i></a>
               <h1>Get In Touch</h1>
               <div class="form-group">
                  <input id="urname" type="text" placeholder="Enter Name" class="form-control req">
               </div>
               <div class="form-group">
                  <input id="urmail" type="email" placeholder="Enter Email" class="form-control req valiEmail">
				  <span id="inVailiEmail" style="color:yellow;"></span>
               </div>
               <div class="form-group">
                  <input id="urmobi" type="text" placeholder="Phone Number" class="form-control req">
                  <span id="inVailiPhone" style="color:yellow;"></span>
               </div>
               <div class="form-group">
                  <select id="urVtype" class="form-control req">
                  	<option value="">Select Type</option>
                    <option value="Student visa">Student visa</option>
                     <option value="Work permit">Work permit</option>
                     <option value="Tourist/Visitor">Tourist/Visitor</option>
                     <option value="Ticket booking">Ticket booking</option>
                  </select>
               </div>
               <div class="form-group">
                  <textarea id="urmsg" placeholder="Comments" class="form-control req"></textarea>
               </div>
               <div class="form-group">
                  <input type="button" class="submitbtn btn btn-primary sub-btn" name="submit" value="Submit">
               </div>
               <div class="form-group"><span id="resData" style="color:yellow;"></span></div>
            </div>
         </div>
      </div>
   </div>



<?php $qry = new WP_Query(array('post_type' => 'myvisabanner')); ?>
		<div class="main-container-banner" style="clear: both;">
         <div id="banner" class="banner_test carousel slide text-center" data-ride="carousel">
            <?php $i = 1; while($qry->have_posts()) : $qry->the_post();
				$desc = get_field('sort_descprition');
				$text = get_field('button_text');
				$url = get_field('button_link');
				$img = get_field('banner_image');
			?>
            <div class="bannerc">
				<img src="<?php echo $img; ?>" class="d-block w-100" alt="<?php the_title();?>" title="<?php the_title();?>" />
                <div class="carousel-caption d-none d-md-block">
					<h5><?php the_title();?></h5>
					<p><?php echo $desc;?></p>
                     <a class="btn btn-info get-in-touch btn-cstm" href="<?php //echo $url; ?>" data-toggle="modal" data-target="#appointment">
						<?php echo $text; ?>
					</a>
                  </div>
               </div>
			   <?php $i++; endwhile; wp_reset_query(); ?>
         </div>
      </div>
<main id="site-content" role="main">
    <div class="floating-info-bx">
      <ul>
         <li><a href="tel:+91 0175 500 0007"><i class="fas fa-phone"></i></a></li>
         <li><a href="mailto:info@myvisa.co.in"><i class="fas fa-envelope"></i></a></li>
         <li><a href="https://api.whatsapp.com/send?phone=+91 0175 500 0007"><i class="fab fa-whatsapp"></i></a></li>
         </ul>
    </div>
	<div id="courseFinder" class="course-finder">
         <div class="container">
            <div class="course-finder-form">
               <div class="mb-4 finder-form">
                  <?php dynamic_sidebar('coursefinder');?>
               </div>
               <form action="http://myvisa.1wayit.com/portal/mysearch" >
                  <div class="row">
                       <div class="col-sm-4">
                        <div class="form-group">
                           <select name="qualification" id="qualific" class="form-control" required>
								<option value="">Please Select</option>
								<option value="+12">+12</option>
								<option value="Graduate">Graduate</option>
								<option value="Post Graduate">Post Graduate</option>
						   </select>
                           <label class="col-form-label text-md-right">Current Qualification</label>
                        </div>
                     </div>
                     <div class="col-sm-4">
                      <div class="form-group">
                         <select name="stream" id="stream" class="form-control">
                           	<option value="">Please Select</option>
                  </select>
                         <label class="col-form-label text-md-right">Stream</label>
                      </div>
                   </div>

                   <div class="col-sm-4">
						<?php $countries = get_all_countries(); ?>
                        <div class="form-group">
                          <select  name="country" class="form-control selectpicker" id="select-country" data-live-search="true" required>
								<option value="">Please Select</option>
								<?php if(is_array($countries)) {
									foreach($countries as $country) { ?>
										<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
								<?php } } ?>
                           </select>
                           <label class="col-form-label text-md-right" style="z-index: 9;">Country</label>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="form-group search-btn-bx">

                           <button class="btn btn-search btn-cstm">Search</button>
                        </div>
                     </div>
                  </div>
               </form>

            </div>
         </div>
      </div>
      <div class="people-gallery">
         <div class="container">
            <div class="row">
               <div class="col-sm-6">
			   <?php $people_talks = get_field('people_talks'); ?>
                  <div class="people-talk-bx">
                     <h3>People Talks</h3>
                     <div id="peopletalk" class="carousel slide" data-ride="carousel1">
						<?php foreach($people_talks as $people){ ?>
						   <div class="carousel-item">
							  <div class="people-talk-video" style="width: 500px;height: 300px;">
                            		<?php $url = $people['Videos'];
                                $url=explode("v=",$url);
                                if(!empty($url[1])){
                                  $urlem=$url[1];
                                }else{
                                  $urlem="";
                                }
										$thumb = $people['thumbnail_image'];

                    //echo do_shortcode('[video_popup url="'.$url.'" img="'.$thumb.'"]' );
									?>
                  <iframe width="300" height="300" src="https://www.youtube.com/embed/<?php echo $urlem;?>" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							  </div>
						   </div>
						<?php  } ?>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6">
					<?php $visa_gallery = get_field('visa_gallery'); ?>
                  <div class="people-talk-bx visa-gallery">
                     <h3>Visa Gallery</h3>
                     <div id="Visa" class="carousel slide" data-ride="carousel">
                        <?php foreach($visa_gallery as $gallery){ ?>
						   <div class="visaitem">
							  <div class="visa-gallery">
								  <img src="<?php echo $gallery['gallery_images'];?>" alt="img" />
							  </div>
						   </div>
						<?php } ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <!--       Our service section-->
      <div class="our-service">
         <div class="free-course-title">
			<?php $our_services = get_field('our_services'); ?>
            <h2><?php echo $our_services['title'];?></h2>
<!--            <p><?php// echo $our_services['small_description'];?></p>-->
         </div>
         <div class="service-div animated wow fadeInDown" data-wow-delay="0.2s">
            <div class="container">
               <div class="row">
				  <?php $qry = new WP_Query(array('post_type' => 'ourservices', 'showposts' => 6, 'orderby' => 'date', 'order' => 'DESC'));
					$ii = 1;
					while($qry->have_posts()) : $qry->the_post();
					$icon = get_field('service_icon');?>
					  <div class="col-md-4" id="service<?php echo $ii++;?>">
						 <div class="ser-div">
							<div class="ser-img">
							   <?php the_post_thumbnail('full', ['class' => 'img-fluid', 'title' => get_the_title()]); ?>
							</div>
							<div class="content-ser">
								<?php if($icon) { ?>
							   <div class="icon-ser"><img src="<?php echo $icon; ?>" class="logo-center"></div>
								<?php } ?>
							   <h3><?php the_title(); ?></h3>
							   <p><?php echo substr(get_the_content(), 0, 150);?></p>
							   <?php if(get_the_ID() == 61) { ?>
								 <a class="read-more" href="<?php echo site_url();?>/form">Apply Now <i class="fa fa-long-arrow-right"></i></a>
							   <?php } else { ?>
								<a class="read-more" href="<?php the_permalink();?>">Apply Now <i class="fa fa-long-arrow-right"></i></a>
							   <?php } ?>
							</div>
						 </div>
					  </div>
				  <?php endwhile; wp_reset_query(); ?>
               </div>
               <div class="mt-3 text-center">
                  <a href="<?php echo site_url();?>/our-services" class="btn btn-cstm btn-service">View All Services</a>
               </div>
            </div>
         </div>
      </div>
      <!--       Our service section-->
      <div class="clearfix"></div>
      <!--       webinar section-->
      <div id="homeBottomForm" class="webnar">
         <div class="container">
		 <?php $upcoming_webinars = get_field('upcoming_webinars');
		 $list = $upcoming_webinars['webinars_list']; ?>
            <div class="row">
               <div class="col-md-6">
                   <div class="upcoming-webinar">
                  <div class="free-course-title">
                     <h2><?php echo $upcoming_webinars['webinar_title']; ?></h2>
                  </div>
				  <div id="upcoming-webinar">
				  <?php if(is_array($list)){
						foreach($list as $single){ ?>
						  <div class="img-div">
							 <div class="content-name">
								<img src="<?php echo $single['background_image']; ?>" class="img-fluid">
								<div class="content-form-img">
								   <h3><?php echo $single['webinar_name']; ?></h3>
									<div class="web-time"><?php echo $single['date_time']; ?></div>
									<div class="expert">
										<img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/dummy.jpg">
									</div>
								   <a href="<?php echo $single['button_link']; ?>" class="btn btn-join btn-info get-in-touch btn-cstm"><?php echo $single['button_text']; ?></a>
								</div>
							 </div>
						  </div>
				  <?php } } ?>
                       </div>
                   </div>
               </div>
               <div class="col-md-6">
                  <div class="submit-form ">
						<div class="contenr-form"><?php $enqForm = get_field('submit_an_enquiry');?>
                        <h2><?php echo $enqForm['title'];?></h2>
<!--                        <p class="mb-0"><?php echo $enqForm['short_description'];?></p>-->
					  </div>
                     <div class="free-course-title">
					<?php //echo do_shortcode('[contact-form-7 id="128" title="Enquiry Form"]'); ?>
          <div role="form" class="wpxcxf7" id="gbn" lang="en-US" dir="ltr">
          <div class="screen-reader-response" aria-live="polite"></div>
          <form action="http://myvisa.1wayit.com/portal/api/common" id="enquiryForm" method="post"  novalidate="novalidate">

          <div class="form-group">
          <h2>Free Immigration Assessment</h2>
          </div>
          <div class="form-group">
          <label>Name *</label><br>
          <span class="wpcf7-form-control-wrap Name"><input type="text" id="name" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Name" required></span>
          </div>
          <div class="form-group">
          <label>Email *</label><br>
          <span class="wpcf7-form-control-wrap Email"><input type="email" id="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" placeholder="Email" required></span>
          </div>
          <div class="form-group">
          <label>Preferred Country </label><br>
          <span class="wpcf7-form-control-wrap destination">
<input type="hidden" name="form_type" value="enquiryForm" />
              <select  name="country" class="form-control selectpicker" id="country" data-live-search="true" required>
            <option value="">Select Country</option><option value="Afghanistan">Afghanistan</option><option value="Albania">Albania</option><option value="Algeria">Algeria</option><option value="American Samoa Andorra Angola">American Samoa Andorra Angola</option><option value="Anguilla">Anguilla</option><option value="Antarctica">Antarctica</option><option value="Antigua and Barbuda">Antigua and Barbuda</option><option value="Argentina Armenia Aruba">Argentina Armenia Aruba</option><option value="Australia">Australia</option><option value="Austria">Austria</option><option value="Azerbaijan">Azerbaijan</option><option value="Bahamas">Bahamas</option><option value="Bahrain">Bahrain</option><option value="Bangladesh">Bangladesh</option><option value="Barbados">Barbados</option><option value="Belarus">Belarus</option><option value="Belgium">Belgium</option><option value="Belize">Belize</option><option value="Benin">Benin</option><option value="Bermuda">Bermuda</option><option value="Bhutan">Bhutan</option><option value="Bolivia">Bolivia</option><option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option><option value="Botswana">Botswana</option><option value="Bouvet">Bouvet</option><option value="Island">Island</option><option value="Brazil">Brazil</option><option value="British">British</option><option value="Indian Ocean Territory">Indian Ocean Territory</option><option value="Brunei">Brunei</option><option value="Darussalam">Darussalam</option><option value="Bulgaria">Bulgaria</option><option value="Burkina">Burkina</option><option value="Faso">Faso</option><option value="Burundi">Burundi</option><option value="Cambodia">Cambodia</option><option value="Cameroon">Cameroon</option><option value="Canada">Canada</option><option value="Cape">Cape</option><option value="Verde">Verde</option><option value="Cayman">Cayman</option><option value="Islands">Islands</option><option value="Central">Central</option><option value="African Republic Chad">African Republic Chad</option><option value="Chile">Chile</option><option value="China">China</option><option value="Christmas Island Cocos (Keeling)">Christmas Island Cocos (Keeling)</option><option value="Islands">Islands</option><option value="Colombia">Colombia</option><option value="Comoros">Comoros</option><option value="Congo">Congo</option><option value="Congo,">Congo,</option><option value="The Democratic Republic of the Cook Islands">The Democratic Republic of the Cook Islands</option><option value="Costa Rica">Costa Rica</option><option value="Cote D'Ivoire">Cote D'Ivoire</option><option value="Croatia">Croatia</option><option value="Cuba">Cuba</option><option value="Cyprus">Cyprus</option><option value="Czech">Czech</option><option value="Republic">Republic</option><option value="Denmark">Denmark</option><option value="Djibouti">Djibouti</option><option value="Dominica">Dominica</option><option value="Dominican">Dominican</option><option value="Republic">Republic</option><option value="Ecuador">Ecuador</option><option value="Egypt">Egypt</option><option value="El Salvador">El Salvador</option><option value="Equatorial">Equatorial</option><option value="Guinea">Guinea</option><option value="Eritrea">Eritrea</option><option value="Estonia">Estonia</option><option value="Ethiopia">Ethiopia</option><option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option><option value="Faroe Islands">Faroe Islands</option><option value="Fiji">Fiji</option><option value="Finland">Finland</option><option value="France">France</option><option value="French">French</option><option value="Guiana">Guiana</option><option value="French">French</option><option value="Polynesia French Southern Territories">Polynesia French Southern Territories</option><option value="Gabon">Gabon</option><option value="Gambia">Gambia</option><option value="Georgia">Georgia</option><option value="Germany">Germany</option><option value="Ghana">Ghana</option><option value="Gibraltar">Gibraltar</option><option value="Greece">Greece</option><option value="Greenland">Greenland</option><option value="Grenada">Grenada</option><option value="Guadeloupe">Guadeloupe</option><option value="Guam">Guam</option><option value="Guatemala">Guatemala</option><option value="Guinea">Guinea</option><option value="Guinea-Bissau">Guinea-Bissau</option><option value="Guyana">Guyana</option><option value="Haiti">Haiti</option><option value="Heard">Heard</option><option value="Island and Mcdonald Islands">Island and Mcdonald Islands</option><option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option><option value="Honduras">Honduras</option><option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option><option value="Iceland">Iceland</option><option value="India">India</option><option value="Indonesia">Indonesia</option><option value="Iran">Iran</option><option value="Islamic Republic of Iraq">Islamic Republic of Iraq</option><option value="Ireland">Ireland</option><option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Jamaica">Jamaica</option><option value="Japan">Japan</option><option value="Jordan">Jordan</option><option value="Kazakhstan">Kazakhstan</option><option value="Kenya">Kenya</option><option value="Kiribati">Kiribati</option><option value="Korea">Korea</option><option value="Democratic People's Republic of Korea,">Democratic People's Republic of Korea,</option><option value="Republic of Kuwait">Republic of Kuwait</option><option value="Kyrgyzstan">Kyrgyzstan</option><option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option><option value="Latvia">Latvia</option><option value="Lebanon">Lebanon</option><option value="Lesotho">Lesotho</option><option value="Liberia">Liberia</option><option value="Libyan">Libyan</option><option value="Arab Jamahiriya">Arab Jamahiriya</option><option value="Liechtenstein">Liechtenstein</option><option value="Lithuania">Lithuania</option><option value="Luxembourg Macao Macedonia,">Luxembourg Macao Macedonia,</option><option value="the Former Yugoslav Republic of Madagascar">the Former Yugoslav Republic of Madagascar</option><option value="Malawi">Malawi</option><option value="Malaysia">Malaysia</option><option value="Maldives">Maldives</option><option value="Mali Malta Marshall">Mali Malta Marshall</option><option value="Islands">Islands</option><option value="Martinique">Martinique</option><option value="Mauritania">Mauritania</option><option value="Mauritius">Mauritius</option><option value="Mayotte">Mayotte</option><option value="Mexico Micronesia">Mexico Micronesia</option><option value="Federated States of Moldova">Federated States of Moldova</option><option value="Republic of Monaco">Republic of Monaco</option><option value="Mongolia">Mongolia</option><option value="Montserrat">Montserrat</option><option value="Morocco">Morocco</option><option value="Mozambique">Mozambique</option><option value="Myanmar">Myanmar</option><option value="Namibia">Namibia</option><option value="Nauru">Nauru</option><option value="Nepal">Nepal</option><option value="Netherlands">Netherlands</option><option value="Netherlands">Netherlands</option><option value="Antilles">Antilles</option><option value="New Caledonia">New Caledonia</option><option value="New Zealand">New Zealand</option><option value="Nicaragua">Nicaragua</option><option value="Niger">Niger</option><option value="Nigeria">Nigeria</option><option value="Niue">Niue</option><option value="Norfolk">Norfolk</option><option value="Island">Island</option><option value="Northern">Northern</option><option value="Mariana">Mariana</option><option value="Islands">Islands</option><option value="Norway">Norway</option><option value="Oman">Oman</option><option value="Pakistan">Pakistan</option><option value="Palau">Palau</option><option value="Palestinian Territory,">Palestinian Territory,</option><option value="Occupied Panama">Occupied Panama</option><option value="Papua New Guinea">Papua New Guinea</option><option value="Paraguay">Paraguay</option><option value="Peru">Peru</option><option value="Philippines">Philippines</option><option value="Pitcairn">Pitcairn</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Puerto">Puerto</option><option value="Rico">Rico</option><option value="Qatar">Qatar</option><option value="Reunion">Reunion</option><option value="Romania">Romania</option><option value="Russian">Russian</option><option value="Federation Rwanda">Federation Rwanda</option><option value="Saint Helena">Saint Helena</option><option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option><option value="Saint Lucia">Saint Lucia</option><option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option><option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option><option value="Samoa San Marino Sao">Samoa San Marino Sao</option><option value="Tome and Principe">Tome and Principe</option><option value="Saudi Arabia Senegal">Saudi Arabia Senegal</option><option value="Serbia and Montenegro">Serbia and Montenegro</option><option value="Seychelles">Seychelles</option><option value="Sierra">Sierra</option><option value="Leone">Leone</option><option value="Singapore">Singapore</option><option value="Slovakia">Slovakia</option><option value="Slovenia">Slovenia</option><option value="Solomon">Solomon</option><option value="Islands">Islands</option><option value="Somalia">Somalia</option><option value="South Africa">South Africa</option><option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option><option value="Spain">Spain</option><option value="Sri Lanka">Sri Lanka</option><option value="Sudan Suriname">Sudan Suriname</option><option value="Svalbard and Jan Mayen Swaziland Sweden">Svalbard and Jan Mayen Swaziland Sweden</option><option value="Switzerland">Switzerland</option><option value="Syrian Arab Republic Taiwan,">Syrian Arab Republic Taiwan,</option><option value="Province of China Tajikistan Tanzania,">Province of China Tajikistan Tanzania,</option><option value="United Republic of Thailand">United Republic of Thailand</option><option value="Timor-Leste Togo">Timor-Leste Togo</option><option value="Tokelau Tonga">Tokelau Tonga</option><option value="Trinidad and Tobago">Trinidad and Tobago</option><option value="Tunisia">Tunisia</option><option value="Turkey Turkmenistan">Turkey Turkmenistan</option><option value="Turks and Caicos Islands">Turks and Caicos Islands</option><option value="Tuvalu">Tuvalu</option><option value="Uganda">Uganda</option><option value="Ukraine">Ukraine</option><option value="United Arab Emirates">United Arab Emirates</option><option value="United Kingdom">United Kingdom</option><option value="United States">United States</option><option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option><option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option><option value="Venezuela">Venezuela</option><option value="Viet Nam Virgin Islands,">Viet Nam Virgin Islands,</option><option value="British Virgin Islands,">British Virgin Islands,</option><option value="U.s. Wallis and Futuna">U.s. Wallis and Futuna</option><option value="Western Sahara">Western Sahara</option><option value="Yemen">Yemen</option><option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option></select></span>
          </div>
          <div class="form-group">
          <label>Service</label><br>
          <span class="wpcf7-form-control-wrap service"><select  name="purpose" id="purpose" class="wpcf7-form-control wpcf7-select form-control" aria-invalid="false"><option value="">Select</option><option value="Visa">Visa</option><option value="Passport">Passport</option><option value="Ticket Booking">Ticket Booking</option><option value="Exam Booking">Exam Booking</option><option value="Medical Booking">Medical Booking</option><option value="Insurance">Insurance</option></select></span>
          </div>
          <div data-id="ticket" data-orig_data_id="ticket" data-clear_on_hide="" data-class="wpcf7cf_group" class="wpcf7cf-hidden">
          <div class="form-group">
          <label>Sub Category</label><br>
          <span class="wpcf7-form-control-wrap sub-service"><select name="visa_type" class="wpcf7-form-control wpcf7-select form-control" aria-invalid="false">
  <option value="">Select</option>
            <option value="Student">Student</option>
            <option value="Tourist">Tourist</option>
          </select></span>
          </div>
          </div>
          <!-- <div data-id="exam_booking" data-orig_data_id="exam_booking" data-clear_on_hide="" data-class="wpcf7cf_group" class="wpcf7cf-hidden">
          <div class="form-group">
          <label>Sub Category</label><br>
          <span class="wpcf7-form-control-wrap sub-service"><select  name="visa_type" class="wpcf7-form-control wpcf7-select form-control" aria-invalid="false"><option value="">Select</option><option value="Student">Student</option><option value="Tourist">Tourist</option></select></span>
          </div>
          </div>
          <div data-id="medical_booking" data-orig_data_id="medical_booking" data-clear_on_hide="" data-class="wpcf7cf_group" class="wpcf7cf-hidden">
          <div class="form-group">
          <label>Sub Category</label><br>
          <span class="wpcf7-form-control-wrap sub-service"><select name="visa_type" class="wpcf7-form-control wpcf7-select form-control" aria-invalid="false"><option value="">Select</option><option value="Student">Student</option><option value="Tourist">Tourist</option></select></span>
          </div>
          </div>
          <div data-id="insurance" data-orig_data_id="insurance" data-clear_on_hide="" data-class="wpcf7cf_group" class="wpcf7cf-hidden">
          <div class="form-group">
          <label>Sub Category</label><br>
          <span class="wpcf7-form-control-wrap sub-service"><select name="visa_type" class="wpcf7-form-control wpcf7-select form-control" aria-invalid="false"><option value="Student">Student</option><option value="Tourist">Tourist</option></select></span>
          </div>
          </div>
          <div data-id="passport" data-orig_data_id="passport" data-clear_on_hide="" data-class="wpcf7cf_group" class="wpcf7cf-hidden">
          <div class="form-group">
          <label>Sub Category</label><br>
          <span class="wpcf7-form-control-wrap sub-service"><select  name="visa_type" class="wpcf7-form-control wpcf7-select form-control" aria-invalid="false"><option value="Student">Student</option><option value="Tourist">Tourist</option></select></span>
          </div>
          </div> -->
          <div data-id="visa" >
          <div class="form-group">
          <label>Sub Category</label><br>
          <span class="wpcf7-form-control-wrap sub-service"><select id="visa_type" name="visa_type" class="wpcf7-form-control wpcf7-select form-control" aria-invalid="false"><option value="">Select</option><option value="Student">Student</option><option value="Tourist">Tourist</option></select></span>
          </div>
          </div>
          <div class="form-group">
            <input type="hidden" id="added_by" name="added_by" value="1" />
            <input type="hidden" id="lead_by" name="lead_by" value="website" />
          <label>Mobile Number *</label><br>
          <span class="wpcf7-form-control-wrap MobileNumber"><input type="number" id="phone" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel form-control" aria-invalid="false" placeholder="Mobile" required></span>
          </div>
          <div class="form-group city-sec">
          <label>Your City</label><br>

          </div>
          <div class="form-group">
          <label>Message</label><br>
          <span class="wpcf7-form-control-wrap Message"><textarea id="message" name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Message / Query"></textarea></span>
          </div>
          <div id="result" class="has-error"></div>
          <div class="btn-submit">
          <input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit btn btn-cstm btn-submit"><span class="ajax-loader"></span>
          </div>
          <div class="wpcf7-response-output wpcf7-display-none" aria-hidden="true"></div></form></div>

                     </div>
				  </div>

               </div>
            </div>
         </div>
      </div>
      <!--      end webinar section-->
      <section class="free-course">
           <div class="container">
		<?php $free_courses = get_field('free_courses'); ?>
         <div class="free-course-title">
            <h2><?php echo $free_courses['top_heading'];?></h2>
<!--            <p><?php// echo $free_courses['small_description'];?></p>-->
         </div>

            <div class="row">
               <div class="col-sm-6">
                  <div class="course-video">
                     <?php echo $free_courses['youtube_video'];?>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="course-content">
                     <h2><?php echo $free_courses['title'];?> </h2>
                     <p><?php echo $free_courses['content'];?>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="testimonial-sec">
         <div class="testimonial-title">
            <h2><?php echo get_field('client_title');?></h2>
<!--            <p><?php// echo get_field('client_description');?></p>-->
         </div>
         <div class="container">
		    <div id="testimonial" class="carousel slide" data-ride="carousel">
				<?php $qry = new WP_Query(array('post_type' => 'clients', 'posts_per_page' => -1));
					while($qry->have_posts()) : $qry->the_post(); ?>
						<div class="col-lg-4 col-12 mt-5 ">
                           <div class="card mt-3 ">
                              <div class="card-header text-center border-0 ">
                                 <div class="row justify-content-center ">
                                    <div class="testi-bx"> <img class="align-self-center text-center rounded-circle outside img-fluid" src="<?php echo get_the_post_thumbnail_url(); ?>" width="110" height="110"> </div>
                                 </div>
                                 <div class=" text-center name">
                                    <div class="testi-bx">
                                       <h5 class="mb-0 profile-pic"><?php the_title();?></h5>
                                       <small class="mt-0"><?php echo get_field('designation');?></small>
                                    </div>
                                 </div>
                              </div>
                              <div class="card-body pt-0 text-center pb-3 ">
                                 <p class="bold"><?php the_content();?></p>
                              </div>
                           </div>
                        </div>
			<?php endwhile; ?>
            </div>
         </div>
      </section>
    <section class="our-partner">
         <div class="container">
			 <?php $parteners = get_field('our_partners', $pid); ?>
             <div class="partner-title">
                  <h2><?php echo $parteners['title'];?></h2>
               </div>
             <div class="partner-detail">
                <?php if(is_array($parteners['logos'])) { ?>
                	<ul id="ourpartnerlogos">
                    	<?php foreach($parteners['logos'] as $logo) {
                            if($logo['logo_image']) { ?>
                   				<li><img src="<?php echo $logo['logo_image']; ?>"></li>
                        	<?php } ?>
                        <?php } ?>
                 	</ul>
                <?php } ?>
               </div>
          </div>
      </section>
   <section class="newsletter-bx">
        <div class="container">
           <div class="newsletter-detail">
               <div class="newsletter-img">
               <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/newletter-ic-img.png"/>
                   </div>
                <h2>Subscribe to the Myvisa Newsletter</h2>
               <div class="news-input-bx">
                   <form>
                   <input class="form-control" type="text" name="email" placeholder="Email"/>
                   <input type="button" class="subscribe-btn btn btn-info get-in-touch btn-cstm" name="submit" value="Subscribe"/>

                    </form>
                 </div>
            </div>
         </div>
       </section>
</main><!-- #site-content -->

 <!-- Modal -->
  <div class="modal fade" id="appointment" role="dialog">
    <div class="modalCenter">
		<div class="modal-dialog">

		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-body">

				<div class="appointment">
					<div class="col-md-12">
						<div class="well-block">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
							<div class="well-title">
								<h2>Book an Appointment</h2>
							</div>
							<form>
								<!-- Form start -->
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label" for="name">Name</label>
											<input id="name" name="name" type="text" placeholder="Name" class="form-control input-md">
											<i class="fa fa-user"></i>
										</div>
									</div>
									<!-- Text input-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label" for="email">Phone</label>
											<input id="phone" name="phone" type="text" placeholder="Phone" class="form-control input-md">
											<i class="fa fa-phone"></i>
										</div>
									</div>
									<!-- Text input-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label" for="email">Email</label>
											<input id="email" name="email" type="email" placeholder="E-Mail" class="form-control input-md">
											<i class="fa fa-envelope"></i>
										</div>
									</div>
									<!-- Text input-->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label" for="date">Preferred Date</label>
											<input id="date" name="date" type="date" placeholder="Preferred Date" class="form-control input-md">
										</div>
									</div>
									<!-- Select Basic -->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label" for="time">Preferred Time</label>
											<select id="time" name="time" class="form-control">
												<option value="8:00 to 9:00">8:00 AM</option>
												<option value="9:00 to 10:00">2:00 PM</option>
												<option value="10:00 to 1:00">5:00 PM</option>
											</select>
										</div>
									</div>
									<!-- Select Basic -->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label" for="appointmentfor">Appointment For</label>
											<select id="appointmentfor" name="appointmentfor" class="form-control">
												<option value="Service#1">Visa</option>
												<option value="Service#2">Passport</option>
												<option value="Service#3">Ticket Booking</option>
												<option value="Service#4">Medical Booking</option>
												<option value="Service#4">Exam Booking</option>
												<option value="Service#4">Insurance</option>
											</select>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label" for="date">Comment</label>
											<textarea id="date" name="date" type="textarea" placeholder="Comment" class="form-control input-md"> </textarea>
										</div>
									</div>
									<!-- Button -->
									<div class="col-md-12">
										<div class="form-group">
											<button id="singlebutton" name="singlebutton" class="btn btn-default">Make An Appointment</button>
										</div>
									</div>
								</div>
							</form>
							<!-- form end -->
						</div>
					</div>
				</div>
			</div>
		  </div>

		</div>
    </div>
  </div>

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
<script type="text/javascript">

$("#enquiryForm").submit(function(event) {

  /* stop form from submitting normally */
  event.preventDefault();

   /* get the action attribute from the <form action=""> element */
  var $form = $(this),
    url = $form.attr('action');

  /* Send the data using post with element id name and name2*/
  var posting = $.post(url, {
    name: $('#name').val(),
    email: $('#email').val(),
    phone: $('#phone').val(),
  added_by: $('#added_by').val(),
    form_type: 'enquiryForm',
    lead_by: $('#lead_by').val(),
    purpose: $('#purpose').val(),
    visa_type: $('#visa_type').val(),
    message: $('#message').val(),
    country: $('#country').val(),

  });

  /* Alerts the results */
  posting.done(function(data) {
    $('#result').html('');
  if(data.success==false)
  { var dat="";
    $.each(data.errors, function (i) {
    $.each(data.errors[i], function (key, val) {
var dat=val;

$('#result').append("<span class='has-error'>"+dat+"</span>");
    });
});


  }else{
   $('#name').val('');
     $('#email').val('');
   $('#phone').val('');

   $('#purpose').val('');
     $('#visa_type').val('');
   $('#message').val('');
   $('#country').val('').trigger("change");


$('#result').append(data.success_message);

  }
  });
  posting.fail(function() {
    $('#result').text('Something went wrong');
  });
});


$(document).on('click','#purpose',function(e){
var ch=$(this).val();
if(ch=='Visa'){
var option="<option value='Arts'>Arts</option><option value='Medical'>Medical</option><option value='Non-Medical'>Non-Medical</option><option value='Commerce'>Commerce</option>";

	$("#stream").html(option);

}
});
$(document).on('click','#qualific',function(e){
var ch=$(this).val();
if(ch=='+12'){
var option="<option value='Arts'>Arts</option><option value='Medical'>Medical</option><option value='Non-Medical'>Non-Medical</option><option value='Commerce'>Commerce</option>";

	$("#stream").html(option);

}
if(ch=='Graduate'){
var option="<option value='B.A.'>B.A.</option><option value='B.Com'>Medical</option><option value='BBA'>BBA</option><option value='B.Tech'>B.Tech</option><option value='B.Sc'>B.Sc</option>";

	$("#stream").html(option);

}

if(ch=='Post Graduate'){
var option="<option value='MBA'>MBA</option><option value='M.Com'>M.Com</option><option value='MA'>MA</option><option value='M.Tech'>M.Tech</option><option value='M.Sc'>M.Sc</option><option value='MBBS'>MBBS</option>";

	$("#stream").html(option);

}

});
</script>
