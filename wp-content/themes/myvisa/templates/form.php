<?php
/**The Template Name: form page
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();  ?>


<main id="site-content" role="main">
   <div class="container">
           <div class="breadcrumbs">
          <ul>
            <li><a href="http://docpoke.in/myvisa/">Our Services</a>>> </li>
            <li>Visa</li>
            </ul>
        </div>
   <div class="loginregister visaForm">

   <ul class="formSteps">
		<li data-id="StepOne" class="SetpOne active">
			<a class="disable" href="javascript:void(0);">Step 1</a>
		</li>
		<li data-id="SetpTwo" class="SetpTwo">
			<a class="disable" href="javascript:void(0);">Step 2</a>
		</li>
	</ul>
  <div class="tab-content">
  		<div id="StepOne" class="tab-pane active LogINTab">
                <div class="service-detail-bxs">
                    <div class="row">
                        <div class="col-sm-6">
                 <div class="ser-div">
					<div class="ser-img">
					 <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-1.jpg">
                     </div>
				<div class="content-ser">
                        <div class="icon-ser">
                            <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/visa.png" class="logo-center"></div>
                            <h3>Visa</h3>
							 </div>
						 </div>
                            </div>
                         <div class="col-sm-6">
                    <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-2.jpg">							</div>
							<div class="content-ser">
							  <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/passport.png" class="logo-center"></div>
							 <h3>Passport</h3>
					</div>
						 </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-3.jpg" >
                               </div>
							<div class="content-ser">
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/ticket.png" class="logo-center"></div>
							<h3>Ticket Booking</h3>
							</div>
						 </div>
                          </div>
                        <div class="col-sm-6">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-4.jpg">
                             </div>
							<div class="content-ser">
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/health.png" class="logo-center"></div>
							 <h3>Medical Booking</h3>
							 </div>
						 </div>
                           </div>
                        <div class="col-sm-6">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exams.jpg" >
                            </div>
							<div class="content-ser">
                                <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exam-1.png" class="logo-center"></div>
							<h3>Exam Booking</h3>
                            </div>
						 </div>
                          </div>
                        <div class="col-sm-6">
                        <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.jpg">
                            </div>
							<div class="content-ser">
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.png" class="logo-center"></div>
						      <h3>Insurance</h3>
							    </div>
						 </div>
                           </div>
                        </div>
            </div>

            <div class="tourist-term-bx">

			  <div class="tourist-form-main">

			   <div class="tourist-form-bx">
				 <div class="row">
						   <div class="col-sm-12">
						  <div class="form-group">
							  <div class="tourist-select-bx">
							  <label>Select your preferred country <img src="<?php echo site_url();?>/wp-content/uploads/2020/06/country-ic.png"/>
</label>
							  <?php $countries = get_all_countries(); ?>
								<select id="choosecountry" name="country" class="form-control selectpicker" id="country" data-live-search="true" required>
								<option value="">Please Select</option>
								<?php if(is_array($countries)) {
								foreach($countries as $country) { ?>
								<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
								<?php } } ?>
								</select>
							</div>
							   </div>
						   </div>
							  <div class="col-sm-12">
						  <div class="form-group">
							   <div class="tourist-select-bx tourist-visa-bx">
								  <label>Type of visa  <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/visa-ic.png"/></label>
								<select id="VisaType" name="visa_type" class="form-control" disabled>
									<option value="">Select Visa</option>

									<option value="Student">Student Visa</option>
									<option value="Tourist">Tourist Visa</option>
								 </select>
							</div>
								  </div>
							   </div>
					   </div>

			   </div>
       <div class="terms-bx">
			<div class="row">
        <div class="col-sm-12">
                       <div class="next-btn">
                     <button type="button" id="nextStep" class="btn btn-search btn-next btn-cstm">Next</button>
                           </div>
                    </div>

</div>

            </div>
    </div>
</div>
		</div>
    <form id="addVisaLeadTourist"  action="http://myvisa.1wayit.com/portal/api/common" name="addVisaLeadTourist" method="post" enctype="multipart/form-data">


		<div id="SetpTwo" class="tab-pane RegTabs Tourist hide">
<input type="hidden" name="visa_type" value="Tourist" />
<input type="hidden" name="country" id="TouristCountry" value="" />
              <div class="service-detail-bxs">
                    <div class="row">
                        <div class="col-sm-12">
                 <div class="ser-div">
					<div class="ser-img">
					 <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-1.jpg">
                     </div>
				<div class="content-ser">
                        <div class="icon-ser">
                            <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/visa.png" class="logo-center"></div>
                            <h3>Visa</h3>
							 </div>
						 </div>
                            </div>
                         <div class="col-sm-12">
                    <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-2.jpg">							</div>
							<div class="content-ser">
							  <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/passport.png" class="logo-center"></div>
							 <h3>Passport</h3>
					</div>
						 </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-3.jpg" >
                               </div>
							<div class="content-ser">
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/ticket.png" class="logo-center"></div>
							<h3>Ticket Booking</h3>
							</div>
						 </div>
                          </div>
                        <div class="col-sm-12">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-4.jpg">
                             </div>
							<div class="content-ser">
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/health.png" class="logo-center"></div>
							 <h3>Medical Booking</h3>
							 </div>
						 </div>
                           </div>
                        <div class="col-sm-12">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exams.jpg" >
                            </div>
							<div class="content-ser">
                                <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exam-1.png" class="logo-center"></div>
							<h3>Exam Booking</h3>
                            </div>
						 </div>
                          </div>
                        <div class="col-sm-12">
                        <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.jpg">
                            </div>
							<div class="content-ser">
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.png" class="logo-center"></div>
						      <h3>Insurance</h3>
							    </div>
						 </div>
                           </div>
                        </div>
            </div>
         
			 <div id="tourist-form-main">
             <div class="eligibilty-form">

                 <h2>Check your Eligibility for visitor/Tourist</h2>


                   <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label class="col-form-label text-md-right">Name <b>*</b></label>

                           <input type="text" name="name" class="form-control" placeholder="">
                            </div>
                          </div>
                           <div class="col-sm-12">
                              <div class="form-group">
                                <label class="col-form-label text-md-right">Email <b>*</b></label>

                               <input type="text" name="email" class="form-control" placeholder="">
                              </div>
                              </div>
                   <div class="col-sm-12">
                          <div class="form-group">
                            <label class="col-form-label text-md-right">Contact Number <b>*</b></label>

                           <input type="number" name="phone" class="form-control" placeholder="">
                          </div>
                          </div>
                           <div class="col-sm-12">
                              <div class="form-group">
                               <input type="text" name="passportNum" class="form-control" placeholder="">
                               <label class="col-form-label text-md-right">Passport Number <b>*</b></label>
                            </div>
                              </div>
                       <div class="col-sm-12">
                          <div class="form-group">
                                 <label class="age d-block">Age Range <b>*</b></label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" value="18-25" name="age" id="ageeight" >
                              <label class="form-check-label" for="ageeight">
                                18-25
                              </label>
                            </div>
                               <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" value="26-30" name="age" id="agesix" >
                              <label class="form-check-label" for="agesix">
                                26-30
                              </label>
                            </div>
                          <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" value="31-45" name="age" id="ageone" >
                              <label class="form-check-label" for="ageone">
                               31-45
                              </label>
                            </div>
                             <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" value="45 &amp; Above" name="Range" id="agefour" >
                              <label class="form-check-label" for="agefour">
                               45 &amp; Above
                              </label>
                            </div>
                           </div>
                         </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                                 <label class="purpose d-block">Purpose of Visit</label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="purpose" id="Tourist" >
                              <label class="form-check-label" for="Tourist">
                               Tourist
                              </label>
                            </div>
                               <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="Range" id="Visitor" >
                              <label class="form-check-label" for="Visitor">
                                Visitor
                              </label>
                            </div>

                           </div>
                         </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                          <select name="occupation" class="form-control">
								<option>Please Select</option>
								<option >Student</option>
								<option >Salaried</option>
								<option >Govt</option>
								<option >Business/Self employed</option>
								<option >Agriculture</option>
								<option >Freelancer</option>
								  </select>
                           <label class="col-form-label text-md-right">Occupation</label>
                        </div>
                          </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                                 <label class="purpose d-block">ITR</label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" value="yes" name="itr" id="yes" >
                              <label class="form-check-label" for="yes">
                               Yes
                              </label>
                            </div>
                               <div class="form-check form-check-inline">
                              <input class="form-check-input" value="no" type="radio" id="itr" name="itr" id="no" >
                              <label class="form-check-label" for="no">
                                No
                              </label>
                            </div>

                           </div>
                         </div>
                        <div class="col-sm-12 incomeyes hide">
                          <div class="form-group">
                                 <label class="purpose d-block">In Case of income yes</label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" value="2.5 L-5.0 L" type="radio" name="incomeyes" id="two" >
                              <label class="form-check-label" for="two">
                               2.5 L-5.0 L
                              </label>
                            </div>
                               <div class="form-check form-check-inline">
                              <input class="form-check-input" value="5.0 L-10.0 L" type="radio" name="incomeyes" id="five">
                              <label class="form-check-label" for="five">
                                5.0 L-10.0 L
                              </label>
                            </div>
                                <div class="form-check form-check-inline">
                              <input class="form-check-input" value="10 L &amp; Above" type="radio" name="incomeyes" id="ten">
                              <label class="form-check-label" for="ten">
                                10 L &amp; Above
                              </label>
                            </div>

                           </div>
                         </div>
                        <div class="col-sm-12 incomeno hide">
                          <div class="form-group">
                                 <label class="purpose d-block">In Case of no other income</label>
                              <div class="income-bx">
                             <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Source of Income </label>
                        </div>
                      </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" value="2.5 L-5.0 L" name="nootherincome" id="two" >
                              <label class="form-check-label" for="two">
                               2.5 L-5.0 L
                              </label>
                            </div>
                               <div class="form-check form-check-inline">
                              <input class="form-check-input" value="5.0 L-10.0 L" type="radio" name="nootherincome" id="five">
                              <label class="form-check-label" for="five">
                                5.0 L-10.0 L
                              </label>
                            </div>
                                <div class="form-check form-check-inline">
                              <input class="form-check-input" value="10 L &amp; Above" type="radio" name="nootherincome" id="ten">
                              <label class="form-check-label" for="ten">
                                10 L &amp; Above
                              </label>
                            </div>

                           </div>
                         </div>
                           <div class="col-sm-12">
                          <div class="form-group">


								<select id="travel-history"  name="tarvelHistory[]" class="form-control selectpicker" multiple data-live-search="true">
								<option value="">Please Select</option>
								<?php if(is_array($countries)) {
								foreach($countries as $country) { ?>
								<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
								<?php } } ?>
								</select>
                              <div class="selected-travel-history">
                                  <div  class="left-side-infor">

                                  </div>
                                      <div class="right-info">
                                       <p>Select all the countries you visited</p>
                                  </div>


                               </div>
                           <label class="col-form-label text-md-right">963.*History</label>
                        </div>
                          </div>
                        <div class="col-sm-12">
                            <div class="refusal-bx">
                          <div class="form-group">
                                 <label class="purpose d-block">Any Refusal</label>
                             <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="Refusalyes" id="Refusalyes" value="yes" >
                              <label class="form-check-label" for="Refusalyes">
                               Yes
                              </label>
                            </div>
                               <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="Refusalyes" value="no" id="Refusalno" >
                              <label class="form-check-label" for="Refusalno">
                                No
                              </label>
                            </div>
                            </div>
                            <div class="form-group refusalYes hide">
                                	<select id="refusalcountry" name="refusalcountry[]" class=" selectpicker" multiple data-live-search="true" >
								<option value="">Please Select</option>
								<?php if(is_array($countries)) {
								foreach($countries as $country) { ?>
								<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
								<?php } } ?>
								</select>
                                <label class="col-form-label text-md-right">Country</label>
                                 <div class="selected-travel-history">
                                  <div  class="left-side-infor">

                                  </div>
                                      <div class="right-info">
                                       <p>Select all the countries you have been refused</p>
                                  </div>

                               </div>
                               </div>

                           </div>
                         </div>
                         <div class="col-sm-12">
                          <div class="form-group">
                                 <label class="purpose d-block">Add Another Family Member</label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" value="yes" name="Memberyes" id="Memberyes" >
                              <label class="form-check-label" for="Memberyes">
                               Yes
                              </label>
                            </div>
                               <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" value="no" name="Memberyes" id="Memberno" >
                              <label class="form-check-label" for="Memberno">
                                No
                              </label>
                            </div>

                           </div>
                         </div>
                          </div>
<div class="row memberYes hide">
  <div class="col-sm-12 ">
       <div class="form-group">
       <select id="anotherMember" name="anotherMember" class="form-control">
  <option value="">Please Select</option>
  <option value="Spouse">Spouse</option>
              <option value="Friend">Friend</option>
             <option value="Parents">Parents</option>
             <option value="Son/Daughter">Son/Daughter</option>
             <option value="Other">Other</option>
  </select>
        <label class="col-form-label text-md-right">Relation</label>
     </div>
       </div>
       <div class="col-sm-12">
         <div class="form-group">
           <label class="col-form-label text-md-right">Name <b>*</b></label>

          <input type="text" name="relName" class="form-control" placeholder="">
           </div>
         </div>
          <div class="col-sm-12">
             <div class="form-group">
               <label class="col-form-label text-md-right">Email <b>*</b></label>

              <input type="text" name="relEmail" class="form-control" placeholder="">
             </div>
             </div>
  <div class="col-sm-12">
         <div class="form-group">
           <label class="col-form-label text-md-right">Contact Number <b>*</b></label>

          <input type="number" name="relPhone" class="form-control" placeholder="">
         </div>
         </div>
          <div class="col-sm-12">
             <div class="form-group">
              <input type="text" name="relPassportNum" class="form-control" placeholder="">
              <label class="col-form-label text-md-right">Passport Number <b>*</b></label>
           </div>
             </div>
      <div class="col-sm-12">
         <div class="form-group">
                <label class="age d-block">Age Range <b>*</b></label>
           <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" value="18-25" name="relAge" id="1ageeight" >
             <label class="form-check-label" for="1ageeight">
               18-25
             </label>
           </div>
              <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" value="26-30" name="relAge" id="1agesix" >
             <label class="form-check-label" for="1agesix">
               26-30
             </label>
           </div>
         <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" value="31-45" name="relAge" id="1ageone" >
             <label class="form-check-label" for="1ageone">
              31-45
             </label>
           </div>
            <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" value="45 &amp; Above" name="relAge" id="1agefour" >
             <label class="form-check-label" for="1agefour">
              45 &amp; Above
             </label>
           </div>
          </div>
        </div>
       <div class="col-sm-12">
         <div class="form-group">
                <label class="purpose d-block">Purpose of Visit</label>
           <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" name="relPurpose" id="realTourist" >
             <label class="form-check-label" for="realTourist">
              Tourist
             </label>
           </div>
              <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" name="relPurpose" id="realVisitor" >
             <label class="form-check-label" for="realVisitor">
               Visitor
             </label>
           </div>

          </div>
        </div>
       <div class="col-sm-12">
         <div class="form-group">
         <select name="relOccupation" class="form-control">
<option value="">Please Select</option>
<option value="Student">Student</option>
<option value="Salaried">Salaried</option>
<option value="Govt">Govt</option>
<option value="Business/Self employed">Business/Self employed</option>
<option value="Agriculture">Agriculture</option>
<option value="Freelancer">Freelancer</option>
 </select>
          <label class="col-form-label text-md-right">Occupation</label>
       </div>
         </div>
       <div class="col-sm-12">
         <div class="form-group">
                <label class="purpose d-block">ITR</label>
           <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" value="yes" name="relItr" id="relyes" >
             <label class="form-check-label" for="relyes">
              Yes
             </label>
           </div>
              <div class="form-check form-check-inline">
             <input class="form-check-input" value="no" type="radio"  name="relItr" id="relno" >
             <label class="form-check-label" for="relno">
               No
             </label>
           </div>

          </div>
        </div>
       <div class="col-sm-12 relincomeyes hide">
         <div class="form-group">
                <label class="purpose d-block">In Case of income yes</label>
           <div class="form-check form-check-inline">
             <input class="form-check-input" value="2.5 L-5.0 L" type="radio" name="relIncomeyes" id="reltwo" >
             <label class="form-check-label" for="reltwo">
              2.5 L-5.0 L
             </label>
           </div>
              <div class="form-check form-check-inline">
             <input class="form-check-input" value="5.0 L-10.0 L" type="radio" name="relIncomeyes" id="relfive">
             <label class="form-check-label" for="relfive">
               5.0 L-10.0 L
             </label>
           </div>
               <div class="form-check form-check-inline">
             <input class="form-check-input" value="10 L &amp; Above" type="radio" name="relIncomeyes" id="relten">
             <label class="form-check-label" for="relten">
               10 L &amp; Above
             </label>
           </div>

          </div>
        </div>
       <div class="col-sm-12 relincomeno hide">
         <div class="form-group">
                <label class="purpose d-block">In Case of no other income</label>
             <div class="income-bx">
            <div class="form-group">
          <input type="text" name="relSource" class="form-control" placeholder="">
          <label class="col-form-label text-md-right">Source of Income </label>
       </div>
     </div>
           <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" value="2.5 L-5.0 L" name="relnootherincome" id="redtentwo" >
             <label class="form-check-label" for="redtentwo">
              2.5 L-5.0 L
             </label>
           </div>
              <div class="form-check form-check-inline">
             <input class="form-check-input" value="5.0 L-10.0 L" type="radio" name="relnootherincome" id="redtenfive">
             <label class="form-check-label" for="redtenfive">
               5.0 L-10.0 L
             </label>
           </div>
               <div class="form-check form-check-inline">
             <input class="form-check-input" value="10 L &amp; Above" type="radio" name="relnootherincome" id="redten">
             <label class="form-check-label" for="redten">
               10 L &amp; Above
             </label>
           </div>

          </div>
        </div>
          <div class="col-sm-12">
         <div class="form-group">


<select id="rel-travel-history" name="relHistory[]" class="form-control selectpicker" multiple data-live-search="true">

<?php if(is_array($countries)) {
foreach($countries as $country) { ?>
<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
<?php } } ?>
</select>
             <div class="selected-travel-history">
                 <div  class="left-side-infor">

                 </div>
                     <div class="right-info">
                      <p>Select all the countries you visited</p>
                 </div>


              </div>
          <label class="col-form-label text-md-right">963.*History</label>
       </div>
         </div>
       <div class="col-sm-12">
           <div class="refusal-bx">
         <div class="form-group">
                <label class="purpose d-block">Any Refusal</label>
            <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" name="relRefusalyes" id="relRefusalyes" value="yes" >
             <label class="form-check-label" for="relRefusalyes">
              Yes
             </label>
           </div>
              <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" name="relRefusalyes" value="no" id="relRefusalno" >
             <label class="form-check-label" for="relRefusalno">
               No
             </label>
           </div>
           </div>
           <div class="form-group relrefusalYes hide">
                 <select id="relrefusalcountry" name="relrefusalcountry[]" class=" selectpicker" multiple data-live-search="true" >
<option value="">Please Select</option>
<?php if(is_array($countries)) {
foreach($countries as $country) { ?>
<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
<?php } } ?>
</select>
               <label class="col-form-label text-md-right">Country</label>
                <div class="selected-travel-history">
                 <div  class="left-side-infor">

                 </div>
                     <div class="right-info">
                      <p>Select all the countries you have been refused</p>
                 </div>

              </div>
              </div>

          </div>
        </div>
        <!-- <div class="col-sm-12">
         <div class="form-group">
                <label class="purpose d-block">Add Another Family Member</label>
           <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" value="yes" name="relMemberyes" id="relMemberyes" >
             <label class="form-check-label" for="relMemberyes">
              Yes
             </label>
           </div>
              <div class="form-check form-check-inline">
             <input class="form-check-input" type="radio" value="no" name="relMemberyes" id="relMemberno" >
             <label class="form-check-label" for="relMemberno">
               No
             </label>
           </div>

          </div>
        </div> -->


                        </div>
                        <div class="row">
						  <div class="col-sm-12">
                                   <div class="form-group">

								<select id="Country-Preference" name="attachment_type" class="form-control" >
								<option value="">Please Select</option>
                                     <option>Adhar card</option>
                                    <option>Passport</option>
                                    <option>Pancard</option>
                                    <option>Photo</option>
                                    <option>Documnet</option>
                                     <option>Bank statement</option>

								</select>
                                    <label class="col-form-label text-md-right">Attachment</label>
                        </div>
                          <div class="form-group custom-file-upload">
                               <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="customFile" name="filename">
                                      <label class="custom-file-label" for="customFile">Choose file</label>
                                       <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                                        <div id="preview_img"></div>
                              </div>

                            </div>
                       </div>

                          <div id="result"></div>
                       <div class="col-sm-12">
                         <!-- <div id="result"></div> -->
                           <div class="sub-btn-sec">
                              <div class="text-center">

                          <button type="submit" class="btn btn-search btn-sub btn-cstm">Submit</button>
                                  </div>
                                  <div id="error_message" class="ajax_response" style="float:left"></div>
	<div id="success_message" class="ajax_response" style="float:left"></div>
                           </div>
                            <div class="terms-bx-list">
                      <h2>Terms &amp; Conditions</h2>
                 <ul>
                  <li>Percentage will be calculated in the behalf of client’s profile.</li>
                  <li>Refusal from Same country will be not eligible.</li>
                  <li>Refusal from US-CAN-UK can apply for any other country.</li>
                  <li>Refusal from any other country can apply for US-CAN-UK.</li>
                 <li>If a visa has been refusal from any country, then we will need the visa form and the refusal letter.</li>
                <li>Refusal from US required DS160 for apply to Canada (Not required for any other country)form – Immigration with MY VISAhttp://docpoke.in 1m 50s.</li>

               </ul>
                      </div>
                       </div>
                        </div>


              </div>


       </div>
                 <div class="video-detail">
                    <iframe width="300" height="315" src="https://www.youtube.com/embed/nWwpyclIEu4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                     <h2>Checklist for Documents:</h2>
                     <p>Lorem Ipsum is simply dummy text in the industry. It to make a type specimen book. It has survived not only five centuries</p>
                     <ul>
                       <li><a href="#"><i class="fa fa-youtube-play"></i> Youtube</a></li>
                       <li><a href="#"><i class="fa fa-file-word-o"></i> Word</a></li>
                       <li><a href="#"><i class="fa fa-file-pdf-o"></i> Pdf</a></li>
                      </ul>
                  </div>

<input type="hidden" name="added_by" value="1">

<input type="hidden" name="lead_by" value="website">
		</div>
</form>
<form id="addVisaLead"  action="http://myvisa.1wayit.com/portal/api/common" name="addVisaLead" method="post" enctype="multipart/form-data">
<input type="hidden" name="visa_type" value="Student" />
<input type="hidden" name="country" id="StudentCountry" value="" />
    <div id="SetpThree" class="tab-pane RegTabs Student hide" style="display: block;">
              <div class="service-detail-bxs">
                    <div class="row">
                        <div class="col-sm-12">
                 <div class="ser-div">
					<div class="ser-img">
					 <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-1.jpg">
                     </div>
				<div class="content-ser">
                        <div class="icon-ser">
                            <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/visa.png" class="logo-center"></div>
                            <h3>Visa</h3>
							 </div>
						 </div>
                            </div>
                         <div class="col-sm-12">
                    <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-2.jpg">							</div>
							<div class="content-ser">
							  <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/passport.png" class="logo-center"></div>
							 <h3>Passport</h3>
					</div>
						 </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-3.jpg">
                               </div>
							<div class="content-ser">
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/ticket.png" class="logo-center"></div>
							<h3>Ticket Booking</h3>
							</div>
						 </div>
                          </div>
                        <div class="col-sm-12">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-4.jpg">
                             </div>
							<div class="content-ser">
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/health.png" class="logo-center"></div>
							 <h3>Medical Booking</h3>
							 </div>
						 </div>
                           </div>
                        <div class="col-sm-12">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exams.jpg">
                            </div>
							<div class="content-ser">
                                <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exam-1.png" class="logo-center"></div>
							<h3>Exam Booking</h3>
                            </div>
						 </div>
                          </div>
                        <div class="col-sm-12">
                        <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.jpg">
                            </div>
							<div class="content-ser">
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.png" class="logo-center"></div>
						      <h3>Insurance</h3>
							    </div>
						 </div>
                           </div>
                        </div>
            </div>
         
			 <div id="tourist-form-main">
             <div class="eligibilty-form student-visa">

                 <h2>Check your Eligibility for Student Visa</h2>
                 <div id="result"></div>
                <form>
                   <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" name="name" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Name <b>*</b></label>
                        </div>
                          </div>
                           <div class="col-sm-12">
                              <div class="form-group">
                               <input type="text" class="form-control" name="email" placeholder="">
                               <label class="col-form-label text-md-right">Email <b>*</b></label>
                            </div>
                              </div>
                   <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" name="phone" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Contact Number <b>*</b></label>
                        </div>
                          </div>
                         <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" name="city" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">City</label>
                        </div>
                          </div>
                         <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" name="state" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">State </label>
                        </div>
                          </div>
                         <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" name="address1" placeholder="">
                           <label class="col-form-label text-md-right">Address Line 01 </label>
                        </div>
                          </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" name="address2" placeholder="">
                           <label class="col-form-label text-md-right">Address Line 02</label>
                        </div>
                          </div>
                           <div class="col-sm-12">
                              <div class="form-group">
                               <input type="text" name="passportNum" class="form-control" placeholder="">
                               <label class="col-form-label text-md-right">Passport Number <b>*</b></label>
                            </div>
                              </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" name="DOB" placeholder="DD/MM/YY">
                           <label class="col-form-label text-md-right">D.O.B</label>
                        </div>
                          </div>
                       <div class="col-sm-12">
                          <div class="form-group">
                                 <label class="age d-block">Relationship Status <b>*</b></label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="relationship" value="single" id="single">
                              <label class="form-check-label" for="single">
                                Single
                              </label>
                            </div>
                               <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="relationship" value="married" id="married">
                              <label class="form-check-label" for="married">
                                Married
                              </label>
                            </div>
                            </div>
                         </div>
                        <div class="col-sm-12">
                          <div class="form-group english-proficency
">
                                 <label class="purpose d-block">English Proficency</label>
                              <div class="check-bx-detail">
                      <div class="custom-control custom-checkbox d-inline-block">
                              <input type="radio" class="custom-control-input" id="IELTS" name="course" value="IELTS">
                              <label class="custom-control-label" for="IELTS">IELTS</label>
                            </div>
                             <div class="custom-control custom-checkbox d-inline-block">
                              <input type="radio" class="custom-control-input" id="PTE" name="course" value="PTE">
                              <label class="custom-control-label" for="PTE">PTE</label>
                            </div>
                      <div class="custom-control custom-checkbox d-inline-block">
                              <input type="radio" class="custom-control-input" id="Duolingo" name="course" value="Duolingo">
                              <label class="custom-control-label" for="Duolingo">Duolingo</label>
                            </div>
                                  <div class="custom-control custom-checkbox d-inline-block">
                              <input type="radio" class="custom-control-input" id="TOEFL" name="course" value="TOEFL">
                              <label class="custom-control-label" for="TOEFL">TOEFL</label>
                            </div>
                            </div>
                              <div class="row">
                               <div class="col-sm-6">
                          <div class="form-group">
                           <input type="number" class="form-control" placeholder="02" name="listening">
                           <label class="col-form-label text-md-right">Listening</label>
                        </div>
                          </div>
                                   <div class="col-sm-6">
                          <div class="form-group">
                           <input type="number" class="form-control" placeholder="03" name="reading">
                           <label class="col-form-label text-md-right">Reading</label>
                        </div>
                          </div>
                                   <div class="col-sm-6">
                          <div class="form-group">
                           <input type="number" class="form-control" placeholder="04" name="writing">
                           <label class="col-form-label text-md-right">Writing</label>
                        </div>
                          </div>
                         <div class="col-sm-6">
                          <div class="form-group">
                           <input type="number" class="form-control" placeholder="03" name="speaking">
                           <label class="col-form-label text-md-right">Speaking</label>
                        </div>
                          </div>
                              <div class="col-sm-12">
                          <div class="form-group">
                           <input type="number" class="form-control" placeholder="03" name="overall">
                           <label class="col-form-label text-md-right">Overall</label>
                        </div>
                          </div>
                              </div>
                              <label class="purpose d-block">Other Tests:</label>
                                <div class="check-bx-detail">
                      <div class="custom-control custom-checkbox d-inline-block">
                              <input type="radio" class="custom-control-input" id="SAT" value="SAT" name="otherTest">
                              <label class="custom-control-label" for="SAT">SAT</label>
                            </div>
                             <div class="custom-control custom-checkbox d-inline-block">
                              <input type="radio" class="custom-control-input" id="GRE" name="otherTest" value="GRE" >
                              <label class="custom-control-label" for="GRE">GRE</label>
                            </div>
                      <div class="custom-control custom-checkbox d-inline-block">
                              <input type="radio" class="custom-control-input" id="GMAT" name="otherTest" value="GMAT">
                              <label class="custom-control-label" for="GMAT">GMAT</label>
                            </div>
                                  <div class="custom-control custom-checkbox d-inline-block">
                              <input type="radio" class="custom-control-input" id="ACT" name="otherTest" value="ACT">
                              <label class="custom-control-label" for="ACT">ACT</label>
                            </div>
                            </div>
                                <div class="row">
                               <div class="col-sm-6">
                          <div class="form-group">
                           <input type="number"   class="form-control" placeholder="02" name="aReasoning">
                           <label class="col-form-label text-md-right"> Reasoning</label>
                        </div>
                          </div>
                                   <div class="col-sm-6">
                          <div class="form-group">
                           <input type="number" class="form-control" placeholder="03" name="verbal">
                           <label class="col-form-label text-md-right">Verbal</label>
                        </div>
                          </div>
                                   <div class="col-sm-6">
                          <div class="form-group">
                           <input type="number" class="form-control" placeholder="04" name="quantitative">
                           <label class="col-form-label text-md-right">Quantitative</label>
                        </div>
                          </div>
                         <div class="col-sm-6">
                          <div class="form-group">
                           <input type="number" class="form-control" placeholder="03" name="totalOther">
                           <label class="col-form-label text-md-right">Total Score</label>
                        </div>
                          </div>

                              </div>
                           </div>
                         </div>

                        <div class="col-sm-12">
                          <div class="form-group">
                                 <label class="purpose d-block">Program Details:</label>

                          <div class="form-group">

                                								<select name="country_visited" id="refusalcountry" class="form-control selectpicker" data-live-search="true" >
								<option value="">Please Select</option>
                <?php if(is_array($countries)) {
                foreach($countries as $country) { ?>
                <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                <?php } } ?>

								</select>
                <select name="country_visited_year" id="refusalYear" class="form-control">
<option value="">Please Select Year</option>
<?php for($i=1990 ; $i<=2020 ; $i++) {?>
<option value="<?php echo $i;?>"><?php echo $i;?></option>
<?php } ?>
</select>

                           <label class="col-form-label text-md-right">Country Preference</label>
                        </div>
                            </div>
                            <div class="city-preference-detail">
                            <div class="row">
                                  <div class="col-sm-12 cityprefer">
                                      <div class="form-group">
                                       <input type="text" name="cityPrefer[]" class="form-control" placeholder="04" >
                                       <label class="col-form-label text-md-right">City Preference</label>
                                           <a  class="city-plus-bx plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                                    </div>

                             </div>
                                    <div class="col-sm-12 proprefer">
                                      <div class="form-group">
                                       <input type="text" class="form-control" placeholder="04" name="proPrefer[]">
                                       <label class="col-form-label text-md-right">Program Preference</label>
                                          <a  class="pro-plus-bx plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                                    </div>

                             </div>
                                    <div class="col-sm-12 uniPrefer">
                                      <div class="form-group">
                                       <input type="text" class="form-control" placeholder="04" name="uniPrefer[]">
                                       <label class="col-form-label text-md-right">College/University Preference</label>
                                               <a  class="uni-plus-bx plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                                    </div>

                             </div>
                      </div>
                       </div>
                         </div>
                      <div class="col-sm-12">
                          <div class="form-group table-bx">
                               <div class="table-responsive">
                                   <h2>Academic Detail   <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a></h2>
                                  <table class="table">
                                     <tbody><tr>
                                       <th>Qualification</th>
                                       <th style="width: 116px;">Years of Completion</th>
                                       <th>Main Subjects</th>
                                       <th>%age</th>
                                       <th>Board/University</th>
                                       </tr>
                                      <tr>
                                       <td>Matriculation</td>
                                       <td><input type="number" name="mYear" value="" /></td>
                                       <td><input type="text" name="mSubject" value="" /></td>
                                       <td><input type="number" name="mPercent" value="" /></td>
                                       <td><input type="text" name="mBoard" value="" /></td>
                                       </tr>
                                       <tr>
                                       <td>Senior Secondary</td>
                                       <td><input type="number" name="sYear" value="" /></td>
                                       <td><input type="text" name="sSubject" value="" /></td>
                                       <td><input type="number" name="sPercent" value="" /></td>
                                       <td><input type="text" name="sBoard" value="" /></td>

                                       </tr>
                                       <tr>
                                       <td>Bachelor's</td>
                                       <td><input type="number" name="bYear" value="" /></td>
                                       <td><input type="text" name="bSubject" value="" /></td>
                                       <td><input type="number" name="bPercent" value="" /></td>
                                       <td><input type="text" name="bBoard" value="" /></td>

                                       </tr>
                                        <tr>
                                       <td>Masters</td>
                                       <td><input type="number" name="maYear" value="" /></td>
                                       <td><input type="text" name="maSubject" value="" /></td>
                                       <td><input type="number" name="maPercent" value="" /></td>
                                       <td><input type="text" name="maBoard" value="" /></td>

                                       </tr>
                                       <tr>
                                       <td>Others</td>
                                       <td><input type="number" name="oYear" value="" /></td>
                                       <td><input type="text" name="oSubject" value="" /></td>
                                       <td><input type="number" name="oPercent" value="" /></td>
                                       <td><input type="text" name="oBoard" value="" /></td>

                                       </tr>
                                    </tbody></table>
                               </div>
                          </div>

                       </div>
                          <div class="col-sm-12">
                          <div class="form-group">
                              <div class="refusal-head-bx">
                                 <label class="purpose d-block">Refusal (If Any):</label>
                          <a class="refusal-plus-bx plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                                  </div>
                              <div class="row refusalPrefer">
                               <div class="col-sm-4">
                          <div class="form-group">

                           <select name="refusalCountry[]" id="refusalcountryq" class="form-control selectpicker" data-live-search="true" >
<option value="">Please Select</option>
<?php if(is_array($countries)) {
foreach($countries as $country) { ?>
<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
<?php } } ?>

</select>

                           <label class="col-form-label text-md-right">Country</label>
                        </div>
                          </div>
                                   <div class="col-sm-4">
                          <div class="form-group">

                           <select name="refusalCountryYear" id="refusalCountryYear" class="form-control">
           <option value="">Please Select Year</option>
           <?php for($i=1990 ; $i<=2020 ; $i++) {?>
           <option value="<?php echo $i;?>"><?php echo $i;?></option>
           <?php } ?>
           </select>

                           <label class="col-form-label text-md-right">Year</label>
                        </div>
                          </div>
                                   <div class="col-sm-4">
                          <div class="form-group">
                           <input type="text" class="form-control refusalCountryDate" placeholder="" name="refusalCountryDate[]">
                           <label class="col-form-label text-md-right">Date</label>
                        </div>
                          </div>
                        </div>
                        <div class="row">
                         <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="" name="gap">
                           <label class="col-form-label text-md-right">Gap</label>
                        </div>
                          </div>
                            <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="" name="workExperience">
                           <label class="col-form-label text-md-right">Work Experience (If Any):</label>
                        </div>
                          </div>
                        <div class="col-sm-12 travelPrefer">
                          <div class="form-group travel-history-info">
                           <input type="text" class="form-control" placeholder="" name="travelHistory[]">
                           <label class="col-form-label text-md-right">Travel Histroy (If Any):</label>
                              <a  class="travel-plus-bx plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                        </div>
                        <input type="hidden" name="lead_by" value="website">
                        <input type="hidden" name="added_by" value="1">

                          </div>
                                 <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="" name="remarks">
                           <label class="col-form-label text-md-right">Remarks (If Any):</label>
                        </div>
                          </div>
                              <div class="col-sm-12">
                                   <div class="form-group">

								<select id="Country-Preferenceq" class="form-control" name="document">
								<option value="">Please Select</option>
                                     <option value="Adhar card">Adhar card</option>
                                    <option value="Passport">Passport</option>
                                    <option value="Pancard">Pancard</option>
                                    <option value="Photo">Photo</option>
                                    <option value="Document">Document</option>
                                     <option value="Bank statement">Bank statement</option>

								</select>
                                    <label class="col-form-label text-md-right">Attachment</label>
                        </div>
                          <div class="form-group custom-file-upload">
                               <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="customFile" name="filename">
                                      <label class="custom-file-label" for="customFile">Choose file</label>
                                       <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                              </div>

                            </div>
                       </div>
                              </div>
                              </div>

                       <div class="col-sm-12">
                           <div class="sub-btn-sec">
                              <div class="text-center">
                          <button type="submit" class="btn btn-search btn-sub btn-cstm">Submit</button>
                                  </div>
                           </div>
                            <div class="terms-bx-list">
                      <h2>Terms &amp; Conditions</h2>
                 <ul>
                  <li>Percentage will be calculated in the behalf of client’s profile.</li>
                  <li>Refusal from Same country will be not eligible.</li>
                  <li>Refusal from US-CAN-UK can apply for any other country.</li>
                  <li>Refusal from any other country can apply for US-CAN-UK.</li>
                 <li>If a visa has been refusal from any country, then we will need the visa form and the refusal letter.</li>
                <li>Refusal from US required DS160 for apply to Canada (Not required for any other country)form – Immigration with MY VISAhttp://docpoke.in 1m 50s.</li>

               </ul>
                      </div>
                              </div>
                       </div>



                              </div>
                  </form>

              </div>


       </div>
                 <div class="video-detail">
                    <iframe width="300" height="315" src="https://www.youtube.com/embed/nWwpyclIEu4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" data-origwidth="300" data-origheight="315" style="width: 690px; height: 724.5px;"></iframe>
                     <h2>Checklist for Documents:</h2>
                     <p>Lorem Ipsum is simply dummy text in the industry. It to make a type specimen book. It has survived not only five centuries</p>
                     <ul>
                       <li><a href="#"><i class="fa fa-youtube-play"></i> Youtube</a></li>
                       <li><a href="#"><i class="fa fa-file-word-o"></i> Word</a></li>
                       <li><a href="#"><i class="fa fa-file-pdf-o"></i> Pdf</a></li>
                      </ul>
                  </div>


		</div>
  </form>
		</div>
		</div>


     </div>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
$(".frmDemo").submit(function(e) {
	e.preventDefault();

		$("#error_message").html("").hide();
		$.ajax({
			type: "POST",
			// url: "post-form.php",
			data: $(this).serialize(),
			success: function(data){
				$('#success_message').fadeIn().html(data);
				setTimeout(function() {
					$('#success_message').fadeOut("slow");
				}, 2000 );

			}
		});

})


jQuery('body').on('click', '.travel-plus-bx', function () {


var block = jQuery(".travelPrefer").clone().attr('id',+jQuery(this).attr("id"));
block.removeClass("travelPrefer");

  block.children('div').find('.travel-plus-bx').append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');

block.children('div').find('.travel-plus-bx').remove();
block.find('.form-group').append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');

block.find('input[type="text"]').val('');
  block.insertAfter("div.travelPrefer:last");
});

jQuery('body').on('click', '.city-plus-bx', function () {


var block = jQuery(".cityprefer").clone().attr('id',+jQuery(this).attr("id"));
block.removeClass("cityprefer");

  block.children('div').find('.city-plus-bx').append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');

block.children('div').find('.city-plus-bx').remove();
block.find('.form-group').append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');

block.find('input[type="text"]').val('');
  block.insertAfter("div.cityprefer:last");
});

jQuery('body').on('click', '.pro-plus-bx', function () {


var block = jQuery(".proprefer").clone().attr('id',+jQuery(this).attr("id"));
block.removeClass("proprefer");

  block.children('div').find('.pro-plus-bx').append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');

block.children('div').find('.pro-plus-bx').remove();
block.find('.form-group').append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');

block.find('input[type="text"]').val('');
  block.insertAfter("div.proprefer:last");
});

jQuery('body').on('click', '.uni-plus-bx', function () {


var block = jQuery(".uniPrefer").clone().attr('id',+jQuery(this).attr("id"));
block.removeClass("uniPrefer");

  block.children('div').find('.uni-plus-bx').append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');

block.children('div').find('.uni-plus-bx').remove();
block.find('.form-group').append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');

block.find('input[type="text"]').val('');
  block.insertAfter("div.uniPrefer:last");
});

jQuery('body').on('click', '.refusal-plus-bx', function () {


var block = jQuery(".refusalPrefer").clone().attr('id',+jQuery(this).attr("id"));
block.removeClass("refusalPrefer");

//   block.children('div').find('.refusal-plus-bx').append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');
//
// block.children('div').find('.refusal-plus-bx').remove();
block.append('<div class="minus_Bx_Icone1"><a class="clone-complete-remove-refusal"><i class="fa fa-minus" aria-hidden="true"></i></a></div>');

block.find('input[type="text"]').val('');
  block.insertAfter("div.refusalPrefer:last");
});

$('body').on('click', '.clone-complete-remove-refusal', function () {

    jQuery(this).closest('.row').remove();
});
$('body').on('click', '.clone-complete-remove', function () {

    jQuery(this).closest('.col-sm-12').remove();
});
$( function() {
   $( "input[name='DOB']" ).datepicker({ maxDate: '0' });
   $( ".refusalCountryDate" ).datepicker({ maxDate: '0' });

 } );
$("#nextStep").click(function(){

  if($("#choosecountry").val()==''){
    return false;
  }
  var visa_type=$("#VisaType").val();
$(".RegTabs").addClass("hide");
$("#"+visa_type+"Country").val($("#choosecountry").val());
$("."+visa_type).removeClass("hide");
});
$("#addVisaLead1").submit(function(event) {

  /* stop form from submitting normally */
  event.preventDefault();

  /* get the action attribute from the <form action=""> element */
  var $form = $(this),
    url = $form.attr('action');
var Data=$( this ).serialize();
console.log(Data);


$.ajax({
    type: "POST",
    url: url,
    data: new FormData($(this)[0]),
    processData: false,
    contentType: false,
    success: function (data) {
      $('#result').html('');
            $('#result').css("color","red");
    if(data.success==false)
    { var dat="";
      $.each(data.errors, function (i) {
      $.each(data.errors[i], function (key, val) {
  var dat=val;

  $('#result').append("<span class='has-error'>"+dat+"</span>");
      });
  });


    }else{
      $('#result').css("color","green");
  $('#result').append(data.success_message);

    }
    }
});


  /* Send the data using post with element id name and name2*/
//   var posting = $.post(url, Data);
//
//   /* Alerts the results */
//   posting.done(function(data) {
//     $('#result').html('');
//   if(data.success==false)
//   { var dat="";
//     $.each(data.errors, function (i) {
//     $.each(data.errors[i], function (key, val) {
// var dat=val;
//
// $('#result').append("<span class='has-error'>"+dat+"</span>");
//     });
// });
//
//
//   }else{
// $('#result').append(data.success_message);
//
//   }
//   });
//   posting.fail(function() {
//     $('#result').text('Something went wrong');
//   });
});

$("input[name='itr']").click(function(){
      var selValue = $("input[name='itr']:checked").val();
      if(selValue=='yes'){
        $(".incomeno").addClass("hide");
$(".incomeyes").removeClass("hide");

}
 if(selValue=='no'){
          $(".incomeyes").addClass("hide");
  $(".incomeno").removeClass("hide");

        }
  });
  $("input[name='relItr']").click(function(){
        var selValue = $("input[name='relItr']:checked").val();
        if(selValue=='yes'){
          $(".relincomeno").addClass("hide");
  $(".relincomeyes").removeClass("hide");

  }
   if(selValue=='no'){
            $(".relincomeyes").addClass("hide");
    $(".relincomeno").removeClass("hide");

          }
    });

  $("input[name='Memberyes']").click(function(){
        var selValue = $("input[name='Memberyes']:checked").val();
        if(selValue=='yes'){

  $(".memberYes").removeClass("hide");

  }
   if(selValue=='no'){
            $(".memberYes").addClass("hide");

          }
    });
    $("input[name='Refusalyes']").click(function(){
          var selValue = $("input[name='Refusalyes']:checked").val();
          if(selValue=='yes'){

    $(".refusalYes").removeClass("hide");

    }
     if(selValue=='no'){
              $(".refusalYes").addClass("hide");

            }
      });

      $("input[name='relRefusalyes']").click(function(){
            var selValue = $("input[name='relRefusalyes']:checked").val();
            if(selValue=='yes'){

      $(".relrefusalYes").removeClass("hide");

      }
       if(selValue=='no'){
                $(".relrefusalYes").addClass("hide");

              }
        });

</script>
<style >.row{width:100%}</style>
