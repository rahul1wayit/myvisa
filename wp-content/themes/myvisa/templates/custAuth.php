<?php
/**The Template Name: User Login
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

if(is_user_logged_in()){
	wp_redirect(home_url()); 
}
get_header();
?>

<main id="site-content" role="main">
	<div class="container">
	    <div class="titleDiv"><h3>User Login</h3></div>
		<div class="loginregister">
		
		<ul class="nav nav-tabs">
		    <li id="LogINTab" class="active"><a data-toggle="tab" href="#login">Login</a></li>
		    <li id="RegTab"><a data-toggle="tab" href="#register">Register</a></li>
		</ul>

		<div class="tab-content">
		<div id="login" class="tab-pane active LogINTab"> 
			<form action="" method="post"> 
				  <div class="row">
					 <div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">E-Mail/Username</label>
						   <input type="text" class="form-control" name="usrnam" id="usrnam" />
						   <span id="emptyusr" class="errmsg"></span>
						   <span id="invalideusr" class="errmsg"></span>
						</div>
					 </div>  
					 <div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">Password</label>
						   <input type="password" class="form-control" name="usrpass" id="usrpass" />
						   <span id="emptypass" class="errmsg"></span>
						</div>
					 </div> 
					 <div class="col-sm-12">
						<div class="form-group" style="position:relative;overflow:hidden;">
						   <button type="button" id="usrlogin" class="btn btn-search btn-cstm">Login</button>
						   <span id="loaderImgLogin" style="display:none;">
								<img src="<?php echo get_template_directory_uri();?>/assets/images/loading.gif" alt="Loader" title="Loader" />
						   </span> 
						</div>
					 </div>
					 <div class="col-sm-12">
						<div class="form-group">
						   <button type="button" id="agentlogin" class="btn btn-search btn-cstm">Agent Login</button> 
						</div>
					 </div>
					 <div class="col-sm-12">
						<p>
						   Don't have an account? <a class="TaBC" data-id="RegTab" href="javascirpt:void(0);">Sign Up Here</a>
						</p>
					 </div>
				  </div>
			</form>
			<div id="responseRes"></div> 
		</div>
		<div id="register" class="tab-pane RegTab">
		  <form id="usrRegForm" name="usrRegForm" action="" method="post">
				  <div class="row">
					 <div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">Name</label>
						   <input type="text" class="form-control req" name="usrnamR" id="usrnamR" />
						</div>
					 </div>  
					 <div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">E-mail</label>
						   <input type="email" class="form-control req" name="usrmail" id="usrmail" />
						   <span id="mailErr"></span>
						</div> 
					 </div>  
					 <div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">Contact Number</label>
						   <input type="text" class="form-control req" name="usrmobile" id="usrmobile" onkeyup="mobiVal();" />
						   <span id="mobiErr"></span> 
						</div>
					 </div>  
					 <div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">City</label>
						   <input type="text" class="form-control req" name="usrcity" id="usrcity" />
						</div>
					 </div>  
					 <div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">State</label>
						   <input type="text" class="form-control req" name="usrstate" id="usrstate" />
						</div>
					 </div>  
					 <!--<div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">Zip Code</label>
						   <input type="text" class="form-control req" name="usrzip" id="usrzip" />
						</div>
					 </div>-->
					 <div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">Password</label>
						   <input type="password" class="form-control req" name="usrpassR" id="usrpassR" />
						</div>
					 </div>
					 <div class="col-sm-12">
						<div class="form-group">
						   <label class="col-form-label text-md-right">Confirm Password</label>
						   <input type="password" class="form-control req" name="usrconpass" id="usrconpass" />
						   <span id="passErr"></span>
						</div>
					 </div>
					 
					 <div class="col-sm-12">
						<div class="form-group" style="position:relative;overflow:hidden;">
						   <button type="button" id="usrreg" class="btn btn-search btn-cstm">Sign Up</button>
						   <span id="loaderImg" style="display:none;">
								<img src="<?php echo get_template_directory_uri();?>/assets/images/loading.gif" alt="Loader" title="Loader" />
						   </span> 
						</div>
					 </div>
					 <div class="col-sm-12">
						<div class="form-group">
						   <button type="button" id="agentlogin" class="btn btn-search btn-cstm">Agent Login</button> 
						</div>
					 </div>
					 <div class="col-sm-12">
						<p>
						   Have an account? <a class="TaBC" data-id="LogINTab" href="javascirpt:void(0);">Login Here</a>
						</p>
					 </div>
				  </div>
			</form>
			<div id="allerr"></div>
			<div id="responseResult"></div> 
		</div>
		</div>
		</div>
	</div>
</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
<?php get_footer(); ?>