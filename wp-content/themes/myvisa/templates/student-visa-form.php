<?php
/**The Template Name: student visa form page
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();  ?>


<main id="site-content" role="main">
   <div class="container">
           <div class="breadcrumbs">
          <ul> 
            <li><a href="http://docpoke.in/myvisa/">Our Services</a>>> </li>
            <li>Visa</li>
            </ul>
        </div>
   <div class="loginregister visaForm">
   
   <ul class="formSteps">
		<li data-id="StepOne" class="SetpOne active">
			<a class="disable" href="javascript:void(0);">Step 1</a>
		</li>
		<li data-id="SetpTwo" class="SetpTwo">
			<a class="disable" href="javascript:void(0);">Step 2</a>
		</li>
	</ul>    
	<div class="tab-content">
		<div id="StepOne" class="tab-pane active LogINTab"> 
                <div class="service-detail-bxs">
                    <div class="row">
                        <div class="col-sm-6">
                 <div class="ser-div">
					<div class="ser-img">
					 <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-1.jpg">		
                     </div>
				<div class="content-ser"> 
                        <div class="icon-ser">
                            <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/visa.png" class="logo-center"></div>
                            <h3>Visa</h3>
							 </div>
						 </div>
                            </div>
                         <div class="col-sm-6">
                    <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-2.jpg">							</div>
							<div class="content-ser"> 
							  <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/passport.png" class="logo-center"></div>
							 <h3>Passport</h3>
					</div>
						 </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-3.jpg" >
                               </div>
							<div class="content-ser"> 
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/ticket.png" class="logo-center"></div>
							<h3>Ticket Booking</h3>
							</div>
						 </div>
                          </div>
                        <div class="col-sm-6">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-4.jpg">						
                             </div>
							<div class="content-ser"> 
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/health.png" class="logo-center"></div>
							 <h3>Medical Booking</h3>
							 </div>
						 </div>
                           </div>
                        <div class="col-sm-6">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exams.jpg" >
                            </div>
							<div class="content-ser"> 
                                <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exam-1.png" class="logo-center"></div>
							<h3>Exam Booking</h3>
                            </div>
						 </div>
                          </div>
                        <div class="col-sm-6">
                        <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.jpg">					
                            </div>
							<div class="content-ser"> 
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.png" class="logo-center"></div>
						      <h3>Insurance</h3>
							    </div>
						 </div>
                           </div>
                        </div>
            </div>
            <div class="tourist-term-bx">
              
			  <div class="tourist-form-main">
                
			   <div class="tourist-form-bx">
					 <form>
					   <div class="row">
						   <div class="col-sm-12">
						  <div class="form-group">
							  <div class="tourist-select-bx">
							  <label>Select your preferred country <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/country-ic.png"/>
</label>
							  <?php $countries = get_all_countries(); ?>
								<select id="choosecountry" class="form-control">
								<option value="">Please Select</option> 
								<?php if(is_array($countries)) {
								foreach($countries as $country) { ?>
								<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
								<?php } } ?>
								</select>
							</div>   
							   </div>
						   </div> 
							  <div class="col-sm-12">
						  <div class="form-group">
							   <div class="tourist-select-bx tourist-visa-bx">
								  <label>Type of visa  <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/visa-ic.png"/></label>
								<select id="VisaType" class="form-control" disabled>
									<option value="">Select Visa</option>
									<option value="Work Visa">Work Visa</option>
									<option value="Student Visa">Student Visa</option>
									<option value="Tourist Visa">Tourist Visa</option>
								 </select>
							</div>  
								  </div>
							   </div>
					   </div> 
					   </form>
			   </div>
       <div class="terms-bx">
			<div class="row">
        <div class="col-sm-12">
                       <div class="next-btn">
                     <button id="nextStep" class="btn btn-search btn-next btn-cstm">Next</button>
                           </div>
                    </div>
                   
</div>

            </div>
    </div>
</div>
		</div>
		<div id="SetpTwo" class="tab-pane RegTab">
              <div class="service-detail-bxs">
                    <div class="row">
                        <div class="col-sm-12">
                 <div class="ser-div">
					<div class="ser-img">
					 <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-1.jpg">		
                     </div>
				<div class="content-ser"> 
                        <div class="icon-ser">
                            <img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/visa.png" class="logo-center"></div>
                            <h3>Visa</h3>
							 </div>
						 </div>
                            </div>  
                         <div class="col-sm-12">
                    <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-2.jpg">							</div>
							<div class="content-ser"> 
							  <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/passport.png" class="logo-center"></div>
							 <h3>Passport</h3>
					</div>
						 </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-3.jpg" >
                               </div>
							<div class="content-ser"> 
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/ticket.png" class="logo-center"></div>
							<h3>Ticket Booking</h3>
							</div>
						 </div>
                          </div>
                        <div class="col-sm-12">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/serv-4.jpg">						
                             </div>
							<div class="content-ser"> 
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/health.png" class="logo-center"></div>
							 <h3>Medical Booking</h3>
							 </div>
						 </div>
                           </div>
                        <div class="col-sm-12">
                            <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exams.jpg" >
                            </div>
							<div class="content-ser"> 
                                <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/exam-1.png" class="logo-center"></div>
							<h3>Exam Booking</h3>
                            </div>
						 </div>
                          </div>
                        <div class="col-sm-12">
                        <div class="ser-div">
							<div class="ser-img">
							   <img width="388" height="255" src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.jpg">					
                            </div>
							<div class="content-ser"> 
                            <div class="icon-ser"><img src="http://docpoke.in/myvisa/wp-content/uploads/2020/06/insurance.png" class="logo-center"></div>
						      <h3>Insurance</h3>
							    </div>
						 </div>
                           </div>
                        </div>
            </div>
			 <div id="tourist-form-main">
             <div class="eligibilty-form student-visa">
                
                 <h2>Check your Eligibility for Student Visa</h2>
                  
                <form>
                   <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Name <b>*</b></label>
                        </div>
                          </div>
                           <div class="col-sm-12">
                              <div class="form-group">
                               <input type="text" class="form-control" placeholder="">
                               <label class="col-form-label text-md-right">Email <b>*</b></label>
                            </div>
                              </div>
                   <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Contact Number <b>*</b></label>
                        </div>
                          </div>
                         <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">City</label>
                        </div>
                          </div>
                         <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">State </label>
                        </div>
                          </div>
                         <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Address Line 01 </label>
                        </div>
                          </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Address Line 02</label>
                        </div>
                          </div>
                           <div class="col-sm-12">
                              <div class="form-group">
                               <input type="text" class="form-control" placeholder="">
                               <label class="col-form-label text-md-right">Passport Number <b>*</b></label>
                            </div>
                              </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="DD/MM/YY">
                           <label class="col-form-label text-md-right">D.O.B</label>
                        </div>
                          </div>
                       <div class="col-sm-12">
                          <div class="form-group">
                                 <label class="age d-block">Relationship Status <b>*</b></label>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="single" id="single" >
                              <label class="form-check-label" for="single">
                                Single
                              </label>
                            </div>
                               <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="married" id="married" >
                              <label class="form-check-label" for="married">
                                Married
                              </label>
                            </div>
                            </div>
                         </div>
                        <div class="col-sm-12">
                          <div class="form-group english-proficency
">
                                 <label class="purpose d-block">English Proficency</label>
                              <div class="check-bx-detail">
                      <div class="custom-control custom-checkbox d-inline-block">
                              <input type="checkbox" class="custom-control-input" id="IELTS" name="IELTS">
                              <label class="custom-control-label" for="IELTS">IELTS</label>
                            </div>
                             <div class="custom-control custom-checkbox d-inline-block">
                              <input type="checkbox" class="custom-control-input" id="PTE" name="PTE">
                              <label class="custom-control-label" for="PTE">PTE</label>
                            </div>
                      <div class="custom-control custom-checkbox d-inline-block">
                              <input type="checkbox" class="custom-control-input" id="Duolingo" name="Duolingo">
                              <label class="custom-control-label" for="Duolingo">Duolingo</label>
                            </div>
                                  <div class="custom-control custom-checkbox d-inline-block">
                              <input type="checkbox" class="custom-control-input" id="TOEFL" name="TOEFL">
                              <label class="custom-control-label" for="TOEFL">TOEFL</label>
                            </div>
                            </div>      
                              <div class="row">
                               <div class="col-sm-6">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="02">
                           <label class="col-form-label text-md-right">Listening</label>
                        </div>
                          </div>
                                   <div class="col-sm-6">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="03">
                           <label class="col-form-label text-md-right">Reading</label>
                        </div>
                          </div>
                                   <div class="col-sm-6">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="04">
                           <label class="col-form-label text-md-right">Writing</label>
                        </div>
                          </div>
                         <div class="col-sm-6">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="03">
                           <label class="col-form-label text-md-right">Speaking</label>
                        </div>
                          </div>
                              <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="03">
                           <label class="col-form-label text-md-right">Overall</label>
                        </div>
                          </div>     
                              </div>
                              <label class="purpose d-block">Other Tests:</label>
                                <div class="check-bx-detail">
                      <div class="custom-control custom-checkbox d-inline-block">
                              <input type="checkbox" class="custom-control-input" id="SAT" name="SAT">
                              <label class="custom-control-label" for="SAT">SAT</label>
                            </div>
                             <div class="custom-control custom-checkbox d-inline-block">
                              <input type="checkbox" class="custom-control-input" id="GRE" name="GRE">
                              <label class="custom-control-label" for="GRE">GRE</label>
                            </div>
                      <div class="custom-control custom-checkbox d-inline-block">
                              <input type="checkbox" class="custom-control-input" id="GMAT" name="GMAT">
                              <label class="custom-control-label" for="GMAT">GMAT</label>
                            </div>
                                  <div class="custom-control custom-checkbox d-inline-block">
                              <input type="checkbox" class="custom-control-input" id="ACT" name="ACT">
                              <label class="custom-control-label" for="ACT">ACT</label>
                            </div>
                            </div>   
                                <div class="row">
                               <div class="col-sm-6">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="02">
                           <label class="col-form-label text-md-right">Analytical Reasoning</label>
                        </div>
                          </div>
                                   <div class="col-sm-6">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="03">
                           <label class="col-form-label text-md-right">Verbal</label>
                        </div>
                          </div>
                                   <div class="col-sm-6">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="04">
                           <label class="col-form-label text-md-right">Quantitative</label>
                        </div>
                          </div>
                         <div class="col-sm-6">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="03">
                           <label class="col-form-label text-md-right">Total Score</label>
                        </div>
                          </div>
                         
                              </div> 
                           </div>
                         </div>
                
                        <div class="col-sm-12">
                          <div class="form-group">
                                 <label class="purpose d-block">Program Details:</label>
                         
                          <div class="form-group">
                              
                                <?php $countries = get_all_countries(); ?>
								<select id="refusalcountry" class="form-control" >
								<option value="">Please Select</option> 
								<?php if(is_array($countries)) {
								foreach($countries as $country) { ?>
								<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
								<?php } } ?> 
                               
								</select>
                                   <ul class="nested-year" style="display:none;">
                                         <li>1990</li>
                                         <li>1991</li>
                                         <li>1992</li>
                                         <li>1993</li>
                                         <li>1994</li>
                                         <li>1995</li>
                                         <li>1996</li>
                                         <li>1997</li>
                                         <li>1998</li>
                                         <li>1999</li>
                                         <li>2000</li>
                                         <li>2001</li>
                                         <li>2002</li>
                                         <li>2003</li>
                                         <li>2004</li>
                                         <li>2005</li>
                                         <li>2006</li>
                                         <li>2007</li>
                                         <li>2008</li>
                                         <li>2009</li>
                                         <li>2010</li>
                                         <li>2011</li>
                                         <li>2012</li>
                                         <li>2013</li>
                                         <li>2014</li>
                                         <li>2015</li>
                                         <li>2016</li>
                                         <li>2017</li>
                                         <li>2018</li>
                                         <li>2019</li>
                                         </ul>
                                    
                              
                              
                              
                              
<!--
     
                              <div class="nested-select-bx">
                                <ul>
                                    <li><span>Afghanistan</span>
                                      <ul class="nested-year">
                                         <li>2000</li>
                                         <li>2001</li>
                                         <li>2002</li>
                                         <li>2003</li>
                                         <li>2004</li>
                                         <li>2005</li>
                                         <li>2006</li>
                                         <li>2007</li>
                                         <li>2008</li>
                                         <li>2009</li>
                                         <li>20010</li>
                                         <li>2011</li>
                                         <li>2012</li>
                                         <li>2013</li>
                                         <li>2014</li>
                                         <li>2015</li>
                                         <li>2016</li>
                                         <li>2017</li>
                                         <li>2018</li>
                                         <li>2019</li>
                                         </ul>
                                    </li>
                                    <li><span>Albania</span>  <ul class="nested-year">
                                         <li>2000</li>
                                         <li>2001</li>
                                         <li>2002</li>
                                         <li>2003</li>
                                         <li>2004</li>
                                         <li>2005</li>
                                         <li>2006</li>
                                         <li>2007</li>
                                         <li>2008</li>
                                         <li>2009</li>
                                         <li>20010</li>
                                         <li>2011</li>
                                         <li>2012</li>
                                         <li>2013</li>
                                         <li>2014</li>
                                         <li>2015</li>
                                         <li>2016</li>
                                         <li>2017</li>
                                         <li>2018</li>
                                         <li>2019</li>
                                         </ul></li>
                                    <li><span>Algeria</span>  <ul class="nested-year">
                                         <li>2000</li>
                                         <li>2001</li>
                                         <li>2002</li>
                                         <li>2003</li>
                                         <li>2004</li>
                                         <li>2005</li>
                                         <li>2006</li>
                                         <li>2007</li>
                                         <li>2008</li>
                                         <li>2009</li>
                                         <li>20010</li>
                                         <li>2011</li>
                                         <li>2012</li>
                                         <li>2013</li>
                                         <li>2014</li>
                                         <li>2015</li>
                                         <li>2016</li>
                                         <li>2017</li>
                                         <li>2018</li>
                                         <li>2019</li>
                                         </ul></li>
                                    <li><span>American Samoa<span>  <ul class="nested-year">
                                         <li>2000</li>
                                         <li>2001</li>
                                         <li>2002</li>
                                         <li>2003</li>
                                         <li>2004</li>
                                         <li>2005</li>
                                         <li>2006</li>
                                         <li>2007</li>
                                         <li>2008</li>
                                         <li>2009</li>
                                         <li>20010</li>
                                         <li>2011</li>
                                         <li>2012</li>
                                         <li>2013</li>
                                         <li>2014</li>
                                         <li>2015</li>
                                         <li>2016</li>
                                         <li>2017</li>
                                         <li>2018</li>
                                         <li>2019</li>
                                         </ul></li>
                                   </ul>  
                              </div>
                        
-->

                              <div class="selected-travel-history">
                                  <div  class="left-side-infor">
                                     <ul>
                                        <li>USA 1992 <a href="#"><i class="fa fa-times"></i></a></li>
                                         <li>India 1993 <a href="#"><i class="fa fa-times"></i></a></li>
                                         <li>Canada 1994 <a href="#"><i class="fa fa-times"></i></a></li>
                                      </ul>
                                  </div>
                                      <div class="right-info">
                                       <p>Select all the countries you visited</p>
                                  </div>
                                      
                                  
                               </div> 
                           <label class="col-form-label text-md-right">Country Preference</label>
                        </div>
                            </div>
                            <div class="city-preference-detail">
                            <div class="row">
                                  <div class="col-sm-12">
                                      <div class="form-group">
                                       <input type="text" class="form-control" placeholder="04">
                                       <label class="col-form-label text-md-right">City Preference</label>
                                           <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                   
                             </div>
                                    <div class="col-sm-12">
                                      <div class="form-group">
                                       <input type="text" class="form-control" placeholder="04">
                                       <label class="col-form-label text-md-right">Program Preference</label>
                                          <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                   
                             </div>
                                    <div class="col-sm-12">
                                      <div class="form-group">
                                       <input type="text" class="form-control" placeholder="04">
                                       <label class="col-form-label text-md-right">College/University Preference</label>
                                               <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                 
                             </div>
                      </div>
                       </div>         
                         </div>
                      <div class="col-sm-12">
                          <div class="form-group table-bx">
                               <div class="table-responsive">
                                   <h2>Academic Detail   <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a></h2> 
                                  <table class="table">
                                     <tr>
                                       <th>Qualification</th>
                                       <th style="width: 116px;">Years of Completion</th>
                                       <th>Main Subjects</th>
                                       <th>%age</th>
                                       <th>Board/University</th>
                                       </tr>
                                      <tr>
                                       <td>Matriculation</td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       </tr>
                                       <tr>
                                       <td>Senior Secondary</td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       </tr>
                                       <tr>
                                       <td>Bachelor's</td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       </tr>
                                        <tr>
                                       <td>Masters</td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       </tr>
                                       <tr>
                                       <td>Others</td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       </tr>
                                    </table>
                               </div>
                          </div>
                      
                       </div>
                          <div class="col-sm-12">
                          <div class="form-group">
                              <div class="refusal-head-bx">
                                 <label class="purpose d-block">Refusal (If Any):</label>
                          <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                                  </div>
                              <div class="row">
                               <div class="col-sm-4">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Country</label>
                        </div>
                          </div>
                                   <div class="col-sm-4">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Year</label>
                        </div>
                          </div>
                                   <div class="col-sm-4">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Date</label>
                        </div>
                          </div>
                         <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Gap</label>
                        </div>
                          </div>
                            <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Work Experience (If Any):</label>
                        </div>
                          </div>    
                        <div class="col-sm-12">
                          <div class="form-group travel-history-info">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Travel Histroy (If Any):</label>
                              <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                        </div>
                          </div>  
                                 <div class="col-sm-12">
                          <div class="form-group">
                           <input type="text" class="form-control" placeholder="">
                           <label class="col-form-label text-md-right">Remarks (If Any):</label>
                        </div>
                          </div>
                              <div class="col-sm-12">
                                   <div class="form-group">
                         
								<select id="Country-Preference" class="form-control" >
								<option value="">Please Select</option> 
                                     <option>Adhar card</option>
                                    <option>Passport</option>
                                    <option>Pancard</option>
                                    <option>Photo</option>
                                    <option>Documnet</option>
                                     <option>Bank statement</option>
							
								</select> 
                                    <label class="col-form-label text-md-right">Attachment</label>
                        </div>
                          <div class="form-group custom-file-upload">
                               <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="customFile" name="filename">
                                      <label class="custom-file-label" for="customFile">Choose file</label>
                                       <a href="#" class="plus-bx">
                                      <i class="fa fa-plus"></i>
                                        </a>
                              </div> 
                              
                            </div> 
                       </div>            
                              </div>
                              </div>
                           
                       <div class="col-sm-12">
                           <div class="sub-btn-sec">
                              <div class="text-center">
                          <button class="btn btn-search btn-sub btn-cstm">Submit</button>
                                  </div> 
                           </div>  
                            <div class="terms-bx-list">
                      <h2>Terms &amp; Conditions</h2>
                 <ul> 
                  <li>Percentage will be calculated in the behalf of client’s profile.</li>    
                  <li>Refusal from Same country will be not eligible.</li>    
                  <li>Refusal from US-CAN-UK can apply for any other country.</li>    
                  <li>Refusal from any other country can apply for US-CAN-UK.</li>   
                 <li>If a visa has been refusal from any country, then we will need the visa form and the refusal letter.</li>  
                <li>Refusal from US required DS160 for apply to Canada (Not required for any other country)form – Immigration with MY VISAhttp://docpoke.in 1m 50s.</li>  
              
               </ul>
                      </div>
                              </div>
                       </div>
                       
                   
                       
                              </div>
                  </form>
              
              </div>
               
                
       </div> 
                 <div class="video-detail">
                    <iframe width="300" height="315" src="https://www.youtube.com/embed/nWwpyclIEu4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                     <h2>Checklist for Documents:</h2>
                     <p>Lorem Ipsum is simply dummy text in the industry. It to make a type specimen book. It has survived not only five centuries</p>
                     <ul>
                       <li><a href="#"><i class="fa fa-youtube-play"></i> Youtube</a></li>
                       <li><a href="#"><i class="fa fa-file-word-o"></i> Word</a></li>
                       <li><a href="#"><i class="fa fa-file-pdf-o"></i> Pdf</a></li>
                      </ul>
                  </div>    
         
            
		</div>
		</div>
		</div>
     
		 
     </div>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
