<?php
/**The Template Name: Passport Page
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
$pid = get_the_ID();
?>
<main id="site-content" role="main">
<!-- Tabs -->
    		<div class="passport">
				<div class="container">
				  <div class="modal-body">
					<div class="well">
					  <h2>Passport Application Form</h2>
					  <ul class="nav nav-tabs">
						<li ><a class="active" href="#login" data-toggle="tab">Passport</a></li>
						<li><a href="#create" data-toggle="tab">Renew Passport</a></li>
					  </ul>
					  <div id="myTabContent" class="tab-content">
						<div class="tab-pane active in" id="login">
						  <form id="tab1">
						   <div class="col-md-6">
								<label>Name </label>
								<input type="text" placeholder="Name"  >
							</div>
							
							<div class="col-md-6">
								<label>Email</label>
								<input type="text" placeholder="Email"  >
							</div>
							
							<div class="col-md-6">
								<label>City</label>
								<input type="text" placeholder="City"  >
							</div>
							
							<div class="col-md-6">
								<label>State</label>
								<input type="text" placeholder="State"  >
							</div>
							
							<div class="col-md-6">
								<label>Pin</label>
								<input type="text" placeholder="Pin"  >
							</div>
							
							<div class="col-md-12">
								<label>Comment</label>
								<textarea placeholder="Comment" ></textarea>
							</div>
		   
							<div class="col-md-12">
							  <button class="btn btn-primary">Submit</button>
							</div>
						  </form>              
						</div>
						<div class="tab-pane fade" id="create">
						  <form id="tab2">
						   <div class="col-md-6">
								<label>Name </label>
								<input type="text" placeholder="Name"  >
							</div>
							
							<div class="col-md-6">
								<label>Email</label>
								<input type="text" placeholder="Email"  >
							</div>
							
							<div class="col-md-6">
								<label>City</label>
								<input type="text" placeholder="City"  >
							</div>
							
							<div class="col-md-6">
								<label>State</label>
								<input type="text" placeholder="State"  >
							</div>
							
							<div class="col-md-6">
								<label>Pin</label>
								<input type="text" placeholder="Pin"  >
							</div>
							<div class="col-md-6">
								<label>Passport Number</label>
								<input type="text" placeholder="Passport Number"  >
							</div>
							
							<div class="col-md-12">
								<label>Comment</label>
								<textarea placeholder="Comment" ></textarea>
							</div>
		   
							<div class="col-md-12">
							  <button class="btn btn-primary">Submit</button>
							</div>
						  </form>
						</div>
					</div>
				  </div>
				</div>
			</div>
        </div>

</main>
<!-- #site-content -->
    

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>