jQuery("#usrlogin").click(function(){
	var usrnam = jQuery("#usrnam").val();
	var usrpass = jQuery("#usrpass").val();
	var err = 0;
	if(usrnam == ''){
		jQuery("#emptyusr").html('Please fill the username.');
		err++;
	}
	if(usrpass == ''){
		jQuery("#emptypass").html('Please fill the password.');
		err++;
	}
	if(err == 0){
		jQuery("#loaderImgLogin").show();
		jQuery.ajax({
         type : "post",
         url : myvisaAjax.ajaxurl,
         data : {action: "usr_login", usrnam : usrnam, usrpass: usrpass},
         success: function(response) {
            jQuery("#responseRes").html(response);
			jQuery("#loaderImgLogin").hide();
         }
      }); 
	}
}); 
/****************** Login Tabs *************************/
jQuery(".loginregister ul.nav.nav-tabs li").click(function(){
	jQuery(".loginregister ul.nav.nav-tabs li").removeClass('active');
	jQuery(this).addClass('active');
	var _did = jQuery(this).attr("id");
	if(_did == 'RegTab'){
		jQuery('#register').show();
		jQuery('#login').hide();
	}else{
		jQuery('#register').hide();
		jQuery('#login').show();		
	}
});
jQuery(".TaBC").click(function(){
	var _id = jQuery(this).attr("data-id");
	jQuery(".loginregister ul.nav.nav-tabs li").removeClass('active');
	jQuery(".loginregister ul.nav.nav-tabs #"+_id).addClass('active');
	jQuery(".tab-content .tab-panel").removeClass('active');
	jQuery(".tab-content .tab-panel."+_id).addClass('active');
	if(_id == 'RegTab'){
		jQuery('#register').show();
		jQuery('#login').hide();
	}else{
		jQuery('#register').hide();
		jQuery('#login').show();		
	}
});

/********************* Register user **********************/
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

jQuery("#usrmobile").on("keypress keyup blur",function (event) {    
	jQuery(this).val(jQuery(this).val().replace(/[^\d].+/, ""));
	if ((event.which < 48 || event.which > 57)) {
			event.preventDefault();
	}
});

jQuery("#usrreg").click(function(){
	var err = 0;
	var data = [];
	jQuery("#responseResult").html('');
	jQuery(".req").each(function(){
		if(jQuery(this).val() == ''){
			jQuery(this).css('border-color', 'red');
			err++;
		}else{
			jQuery(this).css('border-color', '#c4c4c4');
			data[jQuery(this).attr('id')] = jQuery(this).val();
		}
		if(err > 0){
			jQuery("#allerr").html('<div class="alert alert-danger" role="alert">Please fill all fields</div>');
		}
	});
	var chkmail = isEmail(jQuery("#usrmail").val());	
	if(chkmail){
		jQuery("#usrmail").css('border-color', '#c4c4c4');
		jQuery("#mailErr").text(''); 
	}else{
		jQuery("#usrmail").css('border-color', 'red');
		jQuery("#mailErr").text('Invalide email address'); 
		err++;		
	}
	if(jQuery("#usrpassR").val() != jQuery("#usrconpass").val()){
		err++;
		jQuery("#passErr").text("Password does not match");
	}
	if(err > 0){
		return false;
	}else{
		jQuery("#loaderImg").show();
		var _data = jQuery("#usrRegForm").serializeArray();
		jQuery.ajax({
         type : "post",
         url : myvisaAjax.ajaxurl,
         data : {action: "usr_register", fd : _data},
         success: function(response) {
            jQuery("#responseResult").html(response);
			jQuery("#loaderImg").hide();
         }
      }); 
	} 
});


var $cf = jQuery('#usrmobile');
    $cf.blur(function(e){
        var phone = jQuery(this).val();
        phone = phone.replace(/[^0-9]/g,'');
        if (phone.length != 10){
            jQuery('#usrmobile').val('');
			jQuery("#usrmail").css('border-color', 'red');
			jQuery("#mobiErr").text("Mobile number must be 10 digits");
            return false;
        }
    });
	
	
/*********************** Form ********************************/
jQuery("#choosecountry").change(function(){
	var _val = jQuery(this).val();
	if(_val !=''){
		jQuery('#VisaType').removeAttr("disabled");
	}else{
		jQuery('#VisaType').val('').prop("disabled", true);
	}
}); 
jQuery("#nextStep").click(function(){
	var con = jQuery("#choosecountry").val();
	var visa = jQuery("#VisaType").val();
	if(con == '' || visa == ''){
		alert('Please select Country & Visa type.');
	}else{
		jQuery(".formSteps li").removeClass("active");
		jQuery(".formSteps li.SetpTwo").addClass("active");
		jQuery("#StepOne").hide();
		jQuery("#SetpTwo").show(); 
	}
});


/*******************************************************************/
 function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return emailReg.test( $email );
}

jQuery('#ucPopUp').click(function(){
    jQuery(this).hide();
    jQuery(".under-construction-popup").show();
});
jQuery(".under-construction-popup .ucpopup").click(function(){
    jQuery(".under-construction-popup").hide();
    jQuery("#ucPopUp").show();
});

jQuery(".get-in-touch-btn").click(function(){
    jQuery(".get-in-touch-form").show();
});

jQuery(".get-in-touch-form .close").click(function(){
    jQuery(".get-in-touch-form").hide();
});
function phonenumber(inputtxt)
{
  var a = /^\d{10}$/;  
  if (a.test(inputtxt)){
      return true;
  }else{
     return false;
  }
}
jQuery(".submitbtn").click(function(){
    var err=0;
	jQuery(".req").each(function(){
    	if(jQuery(this).val() == ''){
        	jQuery(this).css("border-color", "red");
            err++;
        }else{
        	jQuery(this).css("border-color", "transparent");
        }
    });
    
    if(jQuery(".valiEmail").val() !== ''){
        if(!validateEmail(jQuery(".valiEmail").val())){
            err++;
            jQuery(".valiEmail").css("border-color", "red");
            jQuery("#inVailiEmail").text('Please enter the valid email address');
        }else{
            jQuery("#inVailiEmail").text('');
            jQuery(".valiEmail").css("border-color", "transparent");
        }
    }
    if(jQuery('#urmobi').val() != ''){
        if(!phonenumber(jQuery("#urmobi").val())){
            jQuery("#inVailiPhone").text('Please put 10 digit mobile number');
            jQuery("#urmobi").css("border-color", "red");
            err++;
        }else{
            jQuery("#inVailiPhone").text('');
            jQuery("#urmobi").css("border-color", "transparent");
        } 
    }
    if(err == 0 ){
        var urname = jQuery("#urname").val();
        var urmail = jQuery("#urmail").val();
        var urmobi = jQuery("#urmobi").val();
        var urVtype = jQuery("#urVtype").val();
        var urmsg = jQuery("#urmsg").val();
		var ajaxData = {
            'action': 'startup_mail',
            'urname': urname,
            'urmail': urmail,
            'urmobi': urmobi,
            'urVtype': urVtype,
            'urmsg': urmsg
          }
          jQuery.post(myvisaAjax.ajaxurl, ajaxData, function(response){
                jQuery("#resData").text(response);
          });
    }
});