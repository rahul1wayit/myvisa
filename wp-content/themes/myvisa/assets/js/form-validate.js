$(document).ready(function (){
  //start ready
  var minPhoneLen = 10;
  var maxPhoneLen = 15;
  $.validator.addMethod("noSpace", function(value, element,param)
  {
    return $.trim(value).length >= param;

  }, "No space please and don't leave it empty");
  $.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
  }, "Value must not equal arg.");
  $.validator.addMethod("notEqual", function(value, element, param) {
    return this.optional(element) || value != param;
  }, "This field is required");
  $.validator.addMethod("fullname", function(value, element, param) {
      var patt = new RegExp("^[ A-Za-z]+$");
      var res = patt.test(value);
    return res;
  }, "This field should only contain letters and spaces");
  $.validator.addMethod("phoneformat", function(value, element, param) {
      var phoneno = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
      var patt = value.match(phoneno);
      return patt;
  }, "This field should only contain letters and spaces");
  //Add businesses
  $("#addVisaLead").validate({errorClass: "has-error",highlight: function (element, errorClass) {
    $(element).parents('.form-group').addClass(errorClass);
  },unhighlight:
  function (element, errorClass, validClass)
  {
    $(element).parents('.form-group').removeClass(errorClass);
  },
  rules:{
    name:{
      required: true

    },  email:{
      required: true

    },  phone:{
      required: true

    }

  },
  messages:{
    name: {required: "Name is required."},
    email: {required: "Email is required."},
    mobile: {required: "Mobile is required."},
},
    submitHandler: function (form){

      formSubmit(form);
    }});
    $("#addcourseNew").validate({errorClass: "has-error",highlight: function (element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },unhighlight:
    function (element, errorClass, validClass)
    {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:{
      name:{
        required: true

      },  university_name:{
        required: true

      },  status:{
        required: true

      }, duration:{
        required: true

      },fees:{
        required: true

      },intake:{
        required: true

      },qualification:{
        required: true

      },seat:{
        required: true

      },deadline:{
        required: true

      },
      stream:{
        required: true

      }



    },
    messages:{
      name: {required: "Name is required."},
      university_name: {required: "University is required."},
      status: {required: "Status is required."},
      duration: {required: "Duration is required."},
      fees: {required: "Fees is required."},
      qualification: {required: "Fees is required."},
      seat: {required: "Seat is required."},
      deadline: {required: "Deadline is required."},
        stream: {required: "Stream is required."},

      intake: {required: "Intake is required." }},
      submitHandler: function (form){

        formSubmit(form);
      }});


    $("#editCourse").validate({errorClass: "has-error",highlight: function (element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },unhighlight:
    function (element, errorClass, validClass)
    {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:{
      name:{
        required: true

      },  university_name:{
        required: true

      },  status:{
        required: true

      }, duration:{
        required: true

      },fees:{
        required: true

      },intake:{
        required: true

      }

    },
    messages:{
      name: {required: "Name is required."},
      university_name: {required: "University is required."},
      status: {required: "Status is required."},
      duration: {required: "Duration is required."},
      fees: {required: "Fees is required."},
      intake: {required: "Intake is required." }},
      submitHandler: function (form){

        formSubmit(form);
      }});



$("#commisionUpdate").validate({errorClass: "has-error",highlight: function (element, errorClass) {
  $(element).parents('.form-group').addClass(errorClass);
},unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  commission:{
    required: true

  }

},
messages:{
  commission: {required: "Commision is required."}},
  submitHandler: function (form){

    formSubmit(form);
  }});


  $("#usrRegForm").validate({errorClass: "has-error",highlight: function (element, errorClass) {
    $(element).parents('.form-group').addClass(errorClass);
  },unhighlight:
  function (element, errorClass, validClass)
  {
    $(element).parents('.form-group').removeClass(errorClass);
  },
  rules:{
    name:{
      required: true

    },  email:{
      required: true

    },  mobile:{
      required: true

    }, password:{
      required: true

    },password_confirmation:{
      required: true

    }

  },
  messages:{
    name: {required: "Name is required."},
    email: {required: "Email is required."},
    mobile: {required: "Mobile is required."},
    password: {required: "Password is required."},
    password_confirmation: {required: "Confirm password is required." }},
    submitHandler: function (form){

      formSubmit(form);
    }});

    //Edit Businesses
    //Add businesses
    $("#editbusiness").validate({errorClass: "has-error",highlight: function (element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },unhighlight:
    function (element, errorClass, validClass)
    {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:{
      editname:{
        required: true

      },  editsale_status:{
        required: true

      },  editcommucation:{
        required: true

      }, editfollowup:{
        required: true

      },editrefernumber:{
        required: true

      },editleadsource:{
        required: true

      },editpropertyType:{
        required: true

      },editbroker:{
        required: true

      },editbusinowner:{
        required: true

      }

    },
    messages:{
      editname: {required: "Business name is required."},
      editsale_status: {required: "Sale status is required."},
      editcommucation: {required: "Communication status is required."},
      editfollowup: {required: "Followup is required."},
      editbusinowner: {required: "Business owner is required." },
      editbroker: {required: "Listing Brokers is required."  },
      editleadsource: {required: "Lead Source is required."  },
      editpropertyType:{required: "Property Type is required."  },
      editrefernumber: {required: "Refer Number is required."}},
      submitHandler: function (form){

        formSubmit(form);
      }});

      //Contact Form
      $("#contact").validate({errorClass: "has-error",highlight: function (element, errorClass) {
        $(element).parents('.form-group').addClass(errorClass);
      },unhighlight:
      function (element, errorClass, validClass)
      {
        $(element).parents('.form-group').removeClass(errorClass);
      },
      rules:{
        type:{
          required: true

        },

        email:{
          required: true,noSpace: true,email:true

        },fname:{
          required: true,


        },lname:{
          required: true,


        }
      },
      messages:{
        type: {required: "Contact type is required."
      },

      email: {required: "Email address is required.",email: "Please enter valid email"
    },
    fname: {required: "First name is required.",lettersonly:"Please enter letter only."
  },
  lname: {required: "Last name is required."
}},
submitHandler: function (form){
  formSubmit(form);
}});

//

$("#addVisaLeadTourist").validate({errorClass: "has-error",highlight: function (element, errorClass) {
  $(element).parents('.form-group').addClass(errorClass);
},unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  country:{
    required: true

  },
  visa_type:{
    required: true

  },

  name:{
    required: true,


  },

  email:{
    required: true

  },

  phone:{
    required: true

  }
  ,passportNum:{
    required: true

  },age:{
    required: true

  }


},

messages:{
name: {required: "Name is required.",lettersonly:"Please enter letter only."
},
email: {required: "Email is required."
},
phone: {required: "Contact Number is required."
},
passportNum: {required: "Passport number is required."
},
age: {required: "Age is required."
},
},
submitHandler: function (form){
  formSubmit(form);
}});

//enquiry Form
$("#enquiryForm").validate({errorClass: "has-error",highlight: function (element, errorClass) {
  $(element).parents('.form-group').addClass(errorClass);
},unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  name:{
    required: true,
  },
  email:{
    required: true
  },
  phone:{
    required: true
  },
},

messages:{
name: {required: "Name is required.",lettersonly:"Please enter letter only."
},
email: {required: "Email is required."
},
phone: {required: "Contact Number is required."
},
},
submitHandler: function (form){
  formSubmit(form);
}});


//enquiry Form

//Contact Form
$("#addVisaLead").validate({errorClass: "has-error",highlight: function (element, errorClass) {
  $(element).parents('.form-group').addClass(errorClass);
},unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  country:{
    required: true

  },
  visaType:{
    required: true

  },

  name:{
    required: true,


  },

  email:{
    required: true

  },

  phone:{
    required: true

  }
  ,passportNum:{
    required: true

  },age:{
    required: true

  }


},
messages:{
  country: {required: "Country is required."
},
name: {required: "First name is required.",lettersonly:"Please enter letter only."
},
email: {required: "Email is required."
},
phone: {required: "Phone is required."
},
passportNum: {required: "Passport number is required."
},
age: {required: "Age is required."
}},
submitHandler: function (form){
  formSubmit(form);
}});

//Email Setting Form
$("#changeEmailSetting").validate({errorClass: "has-error",highlight: function (element, errorClass) {$(element).parents('.form-group').addClass(errorClass);},unhighlight: function (element, errorClass, validClass) {$(element).parents('.form-group').removeClass(errorClass);},

rules:
{email:
  {required: true,noSpace: true,email: true}
},
messages:{
  email: {
    required: "Email address is required.",email: "Please enter valid email"
  }},
  submitHandler: function (form){formSubmit(form);}});

  $("#changeTaxRateSettings").validate({errorClass: "has-error",highlight: function (element, errorClass) {$(element).parents('.form-group').addClass(errorClass);},unhighlight: function (element, errorClass, validClass) {$(element).parents('.form-group').removeClass(errorClass);},
  rules:{tax_rate:{required: true,noSpace: true}},
  messages:{tax_rate: {required: "Tax rate is required.",tax_rate: "Please enter valid tax rate"}},
  submitHandler: function (form){formSubmit(form);}});

  $("#changeHourlySetting").validate({errorClass: "has-error",highlight: function (element, errorClass) {$(element).parents('.form-group').addClass(errorClass);},unhighlight: function (element, errorClass, validClass) {$(element).parents('.form-group').removeClass(errorClass);},
  rules:{manhoursofplowingrate:{required: true,noSpace: true},manhoursofsaltingrate:{required: true,noSpace: true},manhoursofsidewalkrate:{required: true,noSpace: true},manhoursofvisitsrate:{required: true,noSpace: true},manhoursofspringrate:{required: true,noSpace: true},manhoursoffallrate:{required: true,noSpace: true},},
  messages:{manhoursofplowingrate: {required: "Man hours of plowing rate field is required."},manhoursofsaltingrate: {required: "Man hours of salting rate field is required."},manhoursofsidewalkrate: {required: "Man hours of sidewalk rate field is required."},manhoursofvisitsrate: {required: "Man hours of visits rate is required."},manhoursofspringrate: {required: "Man hours of spring rate field is required."},manhoursoffallrate: {required: "Man hours of fall rate field is required."}},
  submitHandler: function (form){formSubmit(form);}});

  $("#create_acc_invoice").validate({errorClass: "has-error",highlight: function (element, errorClass) {$(element).parents('.form-group').addClass(errorClass);},unhighlight: function (element, errorClass, validClass) {$(element).parents('.form-group').removeClass(errorClass);},
  rules:{invoice_description:{required: true},invoice_number:{required: true,noSpace: true},invoice_amount:{required: true,noSpace: true},invoice_tax:{required: true,noSpace: true},invoice_total_amount:{required: true,noSpace: true},},
  messages:{invoice_description: {required: "Invoice description field is required."},invoice_number: {required: "Invoice number field is required."},invoice_amount: {required: "Invoie rate field is required."},invoice_tax: {required: "Invoice amount field is required."},invoice_total_amount: {required: "Invoice total amount field is required."}},
  submitHandler: function (form){formSubmit(form);}});

  $("#create_cont_acc_invoice").validate({errorClass: "has-error",highlight: function (element, errorClass) {$(element).parents('.form-group').addClass(errorClass);},unhighlight: function (element, errorClass, validClass) {$(element).parents('.form-group').removeClass(errorClass);},
  rules:{invoice_description:{required: true},invoice_number:{required: true,noSpace: true},invoice_amount:{required: true,noSpace: true},invoice_tax:{required: true,noSpace: true},invoice_total_amount:{required: true,noSpace: true},},
  messages:{invoice_description: {required: "Invoice description field is required."},invoice_number: {required: "Invoice number field is required."},invoice_amount: {required: "Invoie rate field is required."},invoice_tax: {required: "Invoice amount field is required."},invoice_total_amount: {required: "Invoice total amount field is required."}},
  submitHandler: function (form){formSubmit(form);}});


});

function formSubmit(form)
{

 if(form.id != 'create_acc_invoice' && form.id != 'create_cont_acc_invoice'){
    $.ajax({
      url         : form.action,
      type        : form.method,
      data        : new FormData(form),
      headers     : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      contentType : false,
      cache       : false,
      processData : false,
      dataType    : "json",
      beforeSend  : function () {
        $("input[type=submit]").prop("disabled", true);
        $("button[type=submit]").prop("disabled", true);
        $(".loader_div").show();
        $(".loader_div").removeClass("hide");
        $(".loader_div_edit").removeClass("hide");

      },
      complete: function () {
        $(".loader_div").hide();
        $(".loader_div").addClass("hide");
        $(".loader_div_edit").addClass("hide");
        $("input[type=submit]").prop("disabled", false);
        $("button[type=submit]").prop("disabled", false);
      },
      success: function (response) {
        $(".loader_div").hide();
        $(".loader_div").addClass("hide");
        $(".loader_div_edit").addClass("hide");

        $("input[type=submit]").removeAttr("disabled");

        var delayTime = 3000;
        if(response.delayTime)
        delayTime = response.delayTime;
        if (response.success)
        {
          console.log(response.success_message);
          $("#result").html(response.success_message)
          // $.toaster({
          //   priority : 'success',
          //   title    :  'Success',
          //   message  : response.success_message
          // });
          if(response.redirect){
            //setTimeout(function() { window.location.href=response.redirect;}, 1000);
          }

          if( response.updateRecord)
          {
            $.each(response.data, function( index, value )
            {

              $('#tableRow_'+response.data.id).find("td[data-name='"+index+"']").html(value);
            });
          }
          if( response.addRecord)
          {
            $.each(response.data, function( index, value )
            {
              $("input[name='"+index+"']").parents('.form-group').addClass('has-error');
              $("input[name='"+index+"']").after('<label id="'+index+'-error" class="has-error" for="'+index+'">'+value+'</label>');

              $("select[name='"+index+"']").parents('.form-group').addClass('has-error');
              $("select[name='"+index+"']").after('<label id="'+index+'-error" class="has-error" for="'+index+'">'+value+'</label>');
            });
          }
          if (response.modelhide) {
            if (response.delay)
            setTimeout(function (){ $(response.modelhide).modal('hide') },response.delay);
            else
            $(response.modelhide).modal('hide')
          }
        }
        else
        {
          if( response.formErrors)
          {
            var i = 0;
            $.each(response.errors, function( index, value )
            {
              if (i == 0) {
                $("input[name='"+index+"']").focus();
              }
              var str=value.toString();
              if (str.indexOf('edit') != -1) {
                // will not be triggered because str has _..
                value=str.toString().replace('edit', '');
              }


              $("input[name='"+index+"']").parents('.form-group').addClass('has-error');
              $("input[name='"+index+"']").after('<label id="'+index+'-error" class="has-error" for="'+index+'">'+value+'</label>');

              $("textarea[name='"+index+"']").parents('.form-group').addClass('has-error');
              $("textarea[name='"+index+"']").after('<label id="'+index+'-error" class="has-error" for="'+index+'">'+value+'</label>');

              $("select[name='"+index+"']").parents('.form-group').addClass('has-error');
              $("select[name='"+index+"']").after('<label id="'+index+'-error" class="has-error" for="'+index+'">'+value+'</label>');
              i++;
            });
            $("input[type=submit]").removeAttr("disabled");
            $("button[type=submit]").removeAttr("disabled");
          }
          else
          {
            $.toaster({ priority : 'danger',
            title : 'Danger',
            message : response.error_message
          });
        }
      }
      if(response.ajaxPageCallBack)
      {
        response.formid = form.id;
        ajaxPageCallBack(response);
      }
      if(response.resetform)
      {
        $('#'+form.id)[0].reset();
      }
      if(response.submitDisabled)
      {
        $("input[type=submit]").attr("disabled", "disabled");
        $("button[type=submit]").attr("disabled", "disabled");
      }
      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
      if(response.trigger)
      {
        if(response.delayTime){
          setTimeout(function() { $(response.trigger).trigger('click'); $(response.trigger).trigger('change'); $('.modal').modal('hide'); }, response.delayTime);
        }
        else{
          $(response.trigger).trigger('click'); $(response.trigger).trigger('change'); $('.modal').modal('hide');
        }
      }
    },
    error:function(response){
      $.toaster({ priority : 'danger',
      title : 'Danger',
      message : 'Connection error.'
    });
    return false;
  }
});
}
}



//add staff form validation
$("#userLogin").validate({errorClass: "has-error",highlight: function (element, errorClass) {

  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  email:{
    required: true
  },
  password:{
    required: true
  }

},
messages: {

  email: { required: "Email address is required.", email: "Please enter a valid email"
},
password:
{
  required: "Password is required.", number:"Please enter number only"
}
},
submitHandler: function (form){
  formSubmit(form);
}});

//add university//
$("#adduniversity").validate({errorClass: "has-error",highlight: function (element, errorClass) {

  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  name:{
    required: true,
  },
  email:{
    required: true,
    noSpace: true,
    email: true
  },
  phone:{
    required: true,
    phoneformat:true
  },
  city:{
    required: true
  },
  state:{
    required: true
  },
  zipcode:{
    required: true
  },
  country:{
    required: true
  },
  address:{
    required: true
  },
  website:{
    required: true,
    url: true
  },
  status:{
    required: true
  },
},
messages: {
name: { required: "Name is required."},
email:{required: "email is required.",email: "Please enter valid email"},
phone:{required: "phone is required.",phoneformat:"Please enter a valid phone number"},
city:{required: "city is required."},
state:{required: "state is required."},
zipcode:{required: "zipcode is required."},
country:{required: "country is required."},
address:{required: "Address is required."},
website:{required: "website is required.",url: "Please correct url"},
status:{required: "status is required."},
},
submitHandler: function (form){
  formSubmit(form);
}});

//add employee//
$("#addemployee").validate({errorClass: "has-error",highlight: function (element, errorClass) {

  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  name:{
    required: true

  },
  email:{
    required: true,
    noSpace: true,
    email: true
  },
  phone:{
    required: true,
    phoneformat:true
  },
  emp_role:{
    required: true
  },
  password:{
    required: true,
    noSpace: true,
    minlength: 8
  },
 status:{
    required: true
  },
},
messages: {
name: { required: "Name is required.",fullname:"Please enter letter only"},
email:{required: "Email is required.",email: "Please enter valid email"},
phone:{required: "Phone is required.",phoneformat:"Please enter a valid phone number"},
password:
{
  required: "Enter current or new password.", minlength:"Password must have minimum 8 characters"
},
emp_role:{required: "Role is required."},
status:{required: "Status is required."},
},
submitHandler: function (form){
  formSubmit(form);
}});

//add employee//
$("#newAgent").validate({errorClass: "has-error",highlight: function (element, errorClass) {

  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  name:{
    required: true

  },
  email:{
    required: true,
    noSpace: true,
    email: true
  },
  phone:{
    required: true,
    phoneformat:true
  },
  agentType:{
    required: true
  },
  password:{
    required: true,
    noSpace: true,
    minlength: 8
  },
 status:{
    required: true
  }
},
messages: {
name: { required: "Name is required."},
email:{required: "Email is required.",email: "Please enter valid email"},
phone:{required: "Contact number is required.",phoneformat:"Please enter a valid contact number"},
password:
{
  required: "Password is required.", minlength:"Password must have minimum 8 characters"
},
agentType:{required: "Agent type is required."},
status:{required: "Status is required."}
},
submitHandler: function (form){
  formSubmit(form);
}});
//edit university//
$("#edituniversity").validate({errorClass: "has-error",highlight: function (element, errorClass) {

  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  name:{
    required: true,
    fullname:true

  },
  email:{
    required: true,
    noSpace: true,
    email: true
  },
  phone:{
    required: true,
    phoneformat:true
  },
  city:{
    required: true
  },
  state:{
    required: true
  },
  zipcode:{
    required: true
  },
  country:{
    required: true
  },
  address:{
    required: true
  },
  website:{
    required: true,
    url: true
  },
  status:{
    required: true
  },
},
messages: {
name: { required: "Name is required.",fullname:"Please enter letter only"},
email:{required: "Email is required.",email: "Please enter valid email"},
phone:{required: "Phone is required.",phoneformat:"Please enter a valid phone number"},
city:{required: "City is required."},
state:{required: "State is required."},
zipcode:{required: "Zipcode is required."},
country:{required: "Country is required."},
address:{required: "Address is required."},
website:{required: "Website is required.",url: "Please correct url"},
status:{required: "Status is required."},
},
submitHandler: function (form){
  formSubmit(form);
}});

//edit employee//
$("#editemployee").validate({errorClass: "has-error",highlight: function (element, errorClass) {

  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  name:{
    required: true
  },
  email:{
    required: true,
    noSpace: true,
    email: true
  },
  phone:{
    required: true,
    phoneformat:true
  },
  emp_role:{
    required: true
  },
  password:{
    required: true,
    noSpace: true,
    minlength: 8
  },
 status:{
    required: true
  },
},
messages: {
name: { required: "Name is required.",fullname:"Please enter letter only"},
email:{required: "Email is required.",email: "Please enter valid email"},
phone:{required: "Phone is required.",phoneformat:"Please enter a valid phone number"},
password:
{
  required: "Enter current or new password.", minlength:"Password must have minimum 8 characters"
},
emp_role:{required: "Role is required."},
status:{required: "Status is required."},
},
submitHandler: function (form){
  formSubmit(form);
}});

$("#updateuniversity").validate({errorClass: "has-error",highlight: function (element, errorClass) {

  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{},
messages: {},
submitHandler: function (form){
  formSubmit(form);
}});


//delete university//
$("#deleteuniversity").validate({errorClass: "has-error",highlight: function (element, errorClass) {

  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{},
messages: {},
submitHandler: function (form){
  formSubmit(form);
}});

//delete employee//
$("#deleteemployee").validate({errorClass: "has-error",highlight: function (element, errorClass) {

  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{},
messages: {},
submitHandler: function (form){
  formSubmit(form);
}});
// Edit form for employee details

$("#editStaffForm").validate({errorClass: "has-error",highlight: function (element, errorClass) {
  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  first_name:{
    required: true,

  },
  last_name:{
    required: true,

  },
  email:{
    required: true,
    noSpace: true,
    email:true
  },
  mobile:{
    required: true,
    noSpace: true,
    minlength:8,
    maxlength:13,
    number: true
  },
  empPassword:{
    required: true,
    noSpace: true,
    minlength: 8
  },
},
messages: {
  first_name: {
    required: "First name is required.",lettersonly:"Please enter letter only"
  },
  last_name: {
    required: "Last name is required.",lettersonly:"Please enter letter only"
  },
  email: { required: "Email address is required.", email: "Please enter a valid email"
},
mobile:
{
  required: "Mobile number is required.", number:"Please enter number only", minlength:"Mobile number is invalid"
},
empPassword:
{
  required: "Enter current or new password.", minlength:"Password must have minimum 8 characters"
}
},
submitHandler: function (form){
  formSubmit(form);
}});

// Add Task form validation
$("#addTaskForm").validate({errorClass: "has-error",highlight: function (element, errorClass) {
  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  startDate:{
    required: true,
  },
  endDate:{
    required: true,
  },
  priority:{
    required: true,
  }
},
messages:{
  startDate: {required: "The Start Date field is required."},
  endDate: {required: "The End Date field is required."},
  priority: {required: "The Priority field is required."},
},
submitHandler: function (form){
  formSubmit(form);
}});


// Edit Task form validation
$("#editTaskForm").validate({errorClass: "has-error",highlight: function (element, errorClass) {
  $(element).parents('.form-group').addClass(errorClass);
},
unhighlight:
function (element, errorClass, validClass)
{
  $(element).parents('.form-group').removeClass(errorClass);
},
rules:{
  name:{
    required: true,
  },
  startDate:{
    required: true,
  },
  endDate:{
    required: true,
  },
  priority:{
    required: true,
  }
},
messages:{
  name: {required: "This field is required."},
  startDate: {required: "This field is required."},
  endDate: {required: "This field is required."},
  priority: {required: "This field is required."},
},
submitHandler: function (form){
  formSubmit(form);
}});
