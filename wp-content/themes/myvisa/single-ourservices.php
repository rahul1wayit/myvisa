<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
?>
<main id="site-content" role="main">
	<?php
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); ?>
				<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
					<?php
					get_template_part( 'template-parts/entry-header' );
					if ( ! is_search() ) {
						get_template_part( 'template-parts/featured-image' );
					} ?>
					<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">
						<div class="entry-content">
							<div class="Single_event">		
								<div class="section-inner clearfix text-center">
									<h2><?php the_title();?></h2>
									<!-- atart --->
									<div class="overview-main-sec">
										<?php the_content(); ?>
									</div>

									<!-- end --->
								</div>
						</div>	
					</div><!-- .entry-content -->
				</div><!-- .post-inner -->
				<div class="section-inner">
					<?php
						wp_link_pages(
						
						array(
								'before'      => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'twentytwenty' ) . '"><span class="label">' . __( 'Pages:', 'twentytwenty' ) . '</span>',
								'after'       => '</nav>',
								'link_before' => '<span class="page-number">',
								'link_after'  => '</span>',
							)
						);
						edit_post_link();
						// Single bottom post meta.
						twentytwenty_the_post_meta( get_the_ID(), 'single-bottom' );
						if ( is_single() ) {
							get_template_part( 'template-parts/entry-author-bio' );
						} ?>
				</div><!-- .section-inner -->
				<?php
					if ( is_single() ) {
						get_template_part( 'template-parts/navigation' );
					} ?>
				</article><!-- .post -->
			<?php
		} 
	}
?>
</main><!-- #site-content -->
<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
<?php get_footer(); ?>