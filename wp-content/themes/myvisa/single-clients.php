<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
?>
<main id="site-content" role="main">
	<?php
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post(); ?>
				<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
					<?php
					get_template_part( 'template-parts/entry-header' );
					if ( ! is_search() ) {
						get_template_part( 'template-parts/featured-image' );
					}
				$lan = 'en'; ?>
					<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">
						<div class="entry-content">
							<div class="Single_event">		
								<div class="section-inner text-center clearfix">
								  <div class="table_responsive">
									<ul class="client_table">
										<li>
											<strong><?php echo get_option('ctext_when_en');?></strong>
											<?php 
												if (get_post_meta(get_the_ID(), 'date', true) != '') echo date("F j, Y", strtotime(get_post_meta(get_the_ID(), 'date', true))); else echo 'NA';
											?></li>
										<li> 
											<strong><?php echo get_option('ctext_duration_en');?></strong>
											<?php 
												if (get_post_meta(get_the_ID(), 'duration', true) != '') echo get_post_meta(get_the_ID(), 'duration', true); else echo 'NA';
											 ?>
										</li>
										<li>
											<strong><?php echo get_option('text_hb_en');?></strong>
											<?php 
												if (get_post_meta(get_the_ID(), 'guest', true) != '') echo get_post_meta(get_the_ID(), 'guest', true); else echo 'NA';
												echo " Guests";							
											?> 
										</li>
										<li>
											<strong><?php echo get_option('text_where_en');?></strong>
											<?php 
												if (get_post_meta(get_the_ID(), 'location', true) != '') echo get_post_meta(get_the_ID(), 'location', true); else echo 'NA';
											?>
										</li>
										<li><strong><?php echo get_option('ctext_top_en');?></strong><?php 
												if (get_post_meta(get_the_ID(), 'type', true) != '') echo get_post_meta(get_the_ID(), 'type', true); else echo 'NA';							
											?></li>
									</ul>
								</div> 
								<div class="section-inner clearfix text-center">
									<h2><?php the_title();?></h2>
									<!-- atart --->
									<div class="overview-main-sec">
										<?php the_content(); ?>
										<div class="project-detail">
											 <ul>
											   <li><b>PROJECT NAME</b>
												  <span><?php echo (get_field('project_name') != '') ? get_field('project_name') : '-';?></span>
												 </li>
											   <li><b>WEBSITE</b>
												  <span><?php echo (get_field('website') != '') ? get_field('website') : '-';?></span>   
											</li>
											   <li><b>SERVICES</b>
												  <span>Concept, Design, Development</span>
												 </li>
											   <li><b>TYPE</b>
												  <span><?php echo (get_field('type') != '') ? get_field('type') : '-';?></span>
												 </li>
										   
											 </ul>
										</div>
									</div>
									<div class="solution-sec problem-sec">
									   <?php echo get_field('problem');?>
									</div>
									<div class="solution-sec">
									   <?php echo get_field('solutions');?>
									</div>

									<!-- end --->
								</div>
						</div>
					
				<!--<script type="text/javascript" src="<?php //echo get_stylesheet_directory_uri();?>/assets/js/jquery.easing-1.3.pack.js"></script>
				<script type="text/javascript" src="<?php //echo get_stylesheet_directory_uri();?>/assets/js/jquery.mousewheel-3.0.4.pack.js"></script>
				<script type="text/javascript" src="<?php //echo get_stylesheet_directory_uri();?>/assets/js/jquery.fancybox-1.3.4.js"></script>

				<link rel="stylesheet" href="<?php //echo get_stylesheet_directory_uri();?>/assets/js/fancy.css" type="text/css" media="screen" />
				<script>
				jQuery(document).ready(function() {	
					jQuery("a.grouped_elements").fancybox();
				});
				</script>-->	
						<div class="back_Btn text-center">
							<a class="wpcf7-submit btn-submit" href="<?php echo home_url(); ?>/case-studies/"><?php echo  get_option('text_boc_en');?></a>
						</div>	
				<?php
				$related = get_posts( array( 'post_type' => 'casestudy','category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
		if( $related ){ ?>
			<div class="relatedPostsSingle">
				<h2 class="relatedPostTitle">More Case Studies</h2>
				<?php
					foreach( $related as $post ) {
						setup_postdata($post); ?>
						<div class="work_box">
							<a href="<?php the_permalink();?>"><?php the_post_thumbnail();?></a>
							<div class="box-content">
								<?php $cats = get_the_terms( get_the_ID(), 'case_study_category' );  
									if(is_array($cats)){
										foreach($cats as $cat){
											echo '<span class="cats">'. $cat->name .'</span>';
										}
									}
								?>
								<h3 class="title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
								<p><?php $con = get_the_content();
									echo substr($con,0,100);?></p>
							</div>
						</div>
						
			<?php } ?>
			</div>
		<?php }			
		wp_reset_postdata(); ?>
					</div><!-- .entry-content -->
				</div><!-- .post-inner -->
				<div class="section-inner">
					<?php
						wp_link_pages(
						
						array(
								'before'      => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'twentytwenty' ) . '"><span class="label">' . __( 'Pages:', 'twentytwenty' ) . '</span>',
								'after'       => '</nav>',
								'link_before' => '<span class="page-number">',
								'link_after'  => '</span>',
							)
						);
						edit_post_link();
						// Single bottom post meta.
						twentytwenty_the_post_meta( get_the_ID(), 'single-bottom' );
						if ( is_single() ) {
							get_template_part( 'template-parts/entry-author-bio' );
						} ?>
				</div><!-- .section-inner -->
				<?php
					if ( is_single() ) {
						get_template_part( 'template-parts/navigation' );
					} ?>
				</article><!-- .post -->
			<?php
		} 
	}
?>
</main><!-- #site-content -->
<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
<?php get_footer(); ?>