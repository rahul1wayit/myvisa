<?php
/**
 * Getting Started Page
 *
 * @package Blog Designer Pack
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Taking some variables
$show_on_front		= get_option( 'show_on_front' );
$page_for_posts_id	= get_option( 'page_for_posts' );
$page_on_front_id	= get_option( 'page_on_front' );
$new_page_url		= add_query_arg( array('post_type' => 'page', 'post_title' => 'Blog Page', 'content' => '[bdp_post limit="5"]'), admin_url('post-new.php') );
$about_page_url		= add_query_arg( array('page' => 'bdp-about'), admin_url('admin.php') );
$reading_page_url	= admin_url( 'options-reading.php' );
?>
<style type="text/css">
	.bdp-pro-box .hndle{background-color:#0073AA; color:#fff;}
	.bdp-pro-box.postbox{background:#dbf0fa; border:1px solid #0073aa; color:#191e23;}
	.postbox-container .bdp-list li{list-style:square inside;}
	.postbox-container .bdp-list .bdp-tag{display: inline-block; background-color: #fd6448; padding: 1px 5px; color: #fff; border-radius: 3px; font-weight: 600; font-size: 12px;}
	.bdp-wrap .bdp-button-full{display:block; text-align:center; box-shadow:none; border-radius:0;}
	.bdp-box{padding:10px 20px; border-bottom:2px solid #f1f1f1; }
	.bdp-box .bdp-box-content p{ font-size:15px;}
	.bdp-box > h3 span { font-size: 13px;  font-weight: 400;}
	.bdp-box .bdp-box-content span{background:#f1f1f1; font-weight:bold; padding:3px;}
	.bdp-box .bdp-box-content ul { margin: 0 0 5% 0;  background: #F8F8F8; padding: 20px 20px 20px 30px; list-style-type: square; font-size: 15px;  line-height: 1.8;}
	.bdp-notice{background-color: #43AC6A; border-color: #3a945b; color:#fff; font-size:15px; padding:10px; border-radius:5px;}
	.bdp-notice a{color:#fff; text-decoration:underline; font-weight:bold;}
	.bdp-notice span{ font-weight:bold;}
</style>

<div class="wrap bdp-wrap">
	<div id="poststuff">
		<div id="post-body" class="metabox-holder">
			<div id="post-body-content">
				<div class="meta-box-sortables">
					
					<div class="postbox">
						<h3 class="hndle">
							<span><?php _e( 'Getting Started - Blog Designer Pack', 'blog-designer-pack' ); ?></span>
						</h3>
						<div class="inside">
							<div class="bdp-main bdp-box">
								<h3>Success, The Blog Designer Pack is now activated! 😊</h3>
								<div class="bdp-box-content">
									<p>Would you like to create one test blog page to check usage of Blog Designer Pack plugin?</p>
									<p><a class="button button-primary" target="_blank" href="<?php echo esc_url( $new_page_url ); ?>" >Yes, Please do it</a> OR <a class="button button-primary" href="https://docs.infornweb.com/blog-designer-pack/#setup-blog-page" target="_blank" >No, I will configure my self (Give me steps) </a></p>
									<p>To customize the blog page, checkout  <a href="https://docs.infornweb.com/blog-designer-pack/" target="_blank">Our Doc Website</a> where you will find all layouts shortcode plus their parameters like design, category, show author, show date etc.<br />
									If you have any question, please feel free to contact us on <a href="https://wordpress.org/support/plugin/blog-designer-pack/" target="_blank">Support Forum. </a> 
								</div>
							</div>
							
							<div class="bdp-getting-started bdp-box"> 
								<h3>Getting Started <span>(Must Read)</span></h3>
								<div class="bdp-box-content">
									<p>Once you've activated your plugin, you’ll be redirected to this Getting Started page (Blog Designer Pack > Getting Started). Here, you can view the required and helpful steps to use plugin.</p>
									<p>We recommed that please read the below sections for more details.</p>
								</div>
							</div>
							
							<div class="bdp-important-things bdp-box">
								
								<h3>Important things <span>(Required)</span></h3>

								<?php if($show_on_front == "posts") { ?>
									<div class="bdp-post-page">	
										<div class="bdp-notice">
											Your current homepage is set to <span>"Your latest posts"</span>. If you want to customize and change the design of your current blog page with plugin layout and design then kindly go to <a href="<?php echo esc_url( $reading_page_url ); ?>" target="_blank">Settings > Reading</a> and change that selection to <span>"A static page"</span> and then select <span>"Homepage"</span> to any page (that you want to display as a homepage) from the dropdown .
										</div>
										<div class="bdp-box-content">
											<p>We recommed you to refresh this page once you done with above changes.</p>
										</div>
									</div>
								<?php } else if( ! empty( $page_for_posts_id ) ) { ?>
									<div class="bdp-static-page">

										<div class="bdp-notice">
											Your current blog page is set to <span> <?php echo get_the_title( $page_for_posts_id ); ?> </span>. If you want to customize and change the design of your current blog page with plugin layout and design then kindly go to <a href="<?php echo esc_url( $reading_page_url ); ?>" target="_blank">Settings > Reading</a> and change that selection to default one (<strong> " — Select — " </strong>) from the dropdown.
										</div>

										<div class="bdp-box-content">
											<p> Blog page content is handled by WordPress it self.<br />
												To enable Blog Designer Pack plugin design on Blog page, you need to make sure that Blog page should not be selected on <span>posts page</span> of <span>Reading settings</span>. ( <a href="<?php echo esc_url( $reading_page_url ); ?>" target="_blank">Settings > Reading</a>)
											</p>
											<p>First, We recommed you to refresh this page once you done with above changes.</p>
											<p>We recommed you to read the below sections in case if you need more details.</p>
											<ul>
												<li>
													<h4>Blog page is already created</h4>
														If "Blog" page is already created and assigned that page as a <span>Posts page</span> under <a href="<?php echo esc_url( $reading_page_url ); ?>" target="_blank">WordPress Settings > Reading</a> then please change that selection to default one (<strong> " — Select — " </strong>) from the dropdown.
														Once you de-select this setting, open your "Blog" page in edit mode and add the plugin shortcode (Shortcodes are shown under Getting Started section of plugin menu)
												</li>
												<li>
													<h4>Blog page is not created</h4>
														If Blog page is not created then go to Pages > Add New and create a blog page OR some other name as per your need and add the shortcode.
												</li>
											</ul>
											<p>If still you have any question, please feel free to contact us on <a href="https://wordpress.org/support/plugin/blog-designer-pack/" target="_blank">Support Forum. </a> </p>
										</div>
									</div>
								<?php } else { ?>
									<div class="bdp-static-page">
										<div class="bdp-box-content">
											<p>Well done 😊 !!</p>
											<p>Edit your Blog page OR Home page (a static page created by you OR Chosen by you) and add the desired <a href="<?php echo esc_url( $about_page_url ); ?>" target="_blank">shortcode</a> in it.</p>
											<p>If still you have any question, please feel free to contact us on <a href="https://wordpress.org/support/plugin/blog-designer-pack/" target="_blank">Support Forum. </a> </p>
										</div>	
									</div>
								<?php } ?>
							</div>
						</div><!-- .inside -->
					</div><!-- .postbox -->
				
				</div><!-- .meta-box-sortables -->
			</div><!-- #post-body-content -->
			
	</div><!-- #poststuff -->
</div><!-- end .wrap -->